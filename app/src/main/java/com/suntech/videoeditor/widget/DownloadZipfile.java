package com.suntech.videoeditor.widget;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;


import com.suntech.videoeditor.interfaces.ResultUnzip;
import com.suntech.videoeditor.model.Detail;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class DownloadZipfile extends AsyncTask<String, String, String> {
    String result = "";
    private ProgressDialog mProgressDialog;
    String StorezipFileLocation = Environment.getExternalStorageDirectory() + "/DownloadedZip";

    Activity contextParent;
    String DirectoryName;
    ResultUnzip resultUnzip;
    String name;
//    DatabaseHandler db;
    List<Detail> arrSticker;

    public DownloadZipfile(Activity contextParent, String name, List<Detail> arrSticker, String DirectoryName, ResultUnzip resultUnzip) {
        this.contextParent = contextParent;
        this.DirectoryName = DirectoryName;
        this.resultUnzip = resultUnzip;
        this.name = name;
        this.arrSticker = arrSticker;
    }

    public DownloadZipfile(Activity contextParent, String directoryName, String name, ResultUnzip resultUnzip) {
        this.contextParent = contextParent;
        DirectoryName = directoryName;
        this.resultUnzip = resultUnzip;
        this.name = name;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mProgressDialog = new ProgressDialog(contextParent);
        mProgressDialog.setMessage("Downloading...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    @Override
    protected String doInBackground(String... aurl) {
        int count;

        try {
            Log.e("result", aurl[0]);
            URL url = new URL(aurl[0]);
            URLConnection conexion = url.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            InputStream input = new BufferedInputStream(url.openStream());

            OutputStream output = new FileOutputStream(StorezipFileLocation);

            byte data[] = new byte[1024];
            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                output.write(data, 0, count);
            }
            output.close();
            input.close();
            result = "true";
            Log.e("result", result);

        } catch (Exception e) {

            result = "false";
            Log.e("result", result);
        }
        return null;

    }

    protected void onProgressUpdate(String... progress) {
        Log.d("ANDRO_ASYNC", progress[0]);
        mProgressDialog.setProgress(Integer.parseInt(progress[0]));
    }

    @Override
    protected void onPostExecute(String unused) {
        mProgressDialog.dismiss();
        if (result.equalsIgnoreCase("true")) {
            try {
                unzip();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

        }
    }

    public void unzip() throws IOException {
        mProgressDialog = new ProgressDialog(contextParent);
        mProgressDialog.setMessage("Please Wait...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        new UnZipTask().execute(StorezipFileLocation, DirectoryName);
    }

    private class UnZipTask extends AsyncTask<String, Void, Boolean> {
        @SuppressWarnings("rawtypes")
        @Override
        protected Boolean doInBackground(String... params) {
            String filePath = params[0];
            String destinationPath = params[1];

            File archive = new File(filePath);
            try {
                ZipFile zipfile = new ZipFile(archive);
                for (Enumeration e = zipfile.entries(); e.hasMoreElements(); ) {
                    ZipEntry entry = (ZipEntry) e.nextElement();
                    unzipEntry(zipfile, entry, destinationPath);
                }


                UnzipUtil d = new UnzipUtil(StorezipFileLocation, DirectoryName);
                d.unzip();

            } catch (Exception e) {
                return false;
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean result) {
//            db = new DatabaseHandler(contextParent);
//            db.addContacts(new SaveName(name));
            mProgressDialog.dismiss();
            resultUnzip.dataUnzip(DirectoryName);
        }


        private void unzipEntry(ZipFile zipfile, ZipEntry entry, String outputDir) throws IOException {
            if (entry.isDirectory()) {
                createDir(new File(outputDir, entry.getName()));
                return;
            }

            File outputFile = new File(outputDir, entry.getName());
            if (!outputFile.getParentFile().exists()) {
                createDir(outputFile.getParentFile());
            }

            // Log.v("", "Extracting: " + entry);
            BufferedInputStream inputStream = new BufferedInputStream(zipfile.getInputStream(entry));
            BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

            try {

            } finally {
                outputStream.flush();
                outputStream.close();
                inputStream.close();
            }
        }

        private void createDir(File dir) {
            if (dir.exists()) {
                return;
            }
            if (!dir.mkdirs()) {
                throw new RuntimeException("Can not create dir " + dir);
            }
        }
    }
}
