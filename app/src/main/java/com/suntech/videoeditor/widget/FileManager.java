package com.suntech.videoeditor.widget;

import java.io.File;

public class FileManager {
    public static int countFile(String directoryName) {
        int count = 0;
        File directory = new File(directoryName);
        File[] files = new File[0];
        try {
            files = directory.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].getName().contains("_MACOSX") || files[i].getName().contains("config.json")) {
                } else {
                    count++;
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return count;
    }
}
