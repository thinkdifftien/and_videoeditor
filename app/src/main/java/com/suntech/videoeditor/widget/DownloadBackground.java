package com.suntech.videoeditor.widget;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadBackground extends AsyncTask<String, String, String> {

    String result = "";
    Activity contextParent;
    private File fileDirect;

    public DownloadBackground(Activity contextParent) {
        this.contextParent = contextParent;
    }

    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground (String...aurl)
    {
        int count;

        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/VideoEditor");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }

            fileDirect = new File (myDir, "background.png");
            Log.e("background",fileDirect.getAbsolutePath());
            URL url = new URL(aurl[0]);
            URLConnection conexion = url.openConnection();
            conexion.connect();
            int lenghtOfFile = conexion.getContentLength();
            InputStream input = new BufferedInputStream(url.openStream());

            OutputStream output = new FileOutputStream(fileDirect.getAbsoluteFile());

            byte data[] = new byte[1024];
            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                output.write(data, 0, count);
            }
            output.close();
            input.close();
            result = "true";

        } catch (Exception e) {

            result = "false";
        }
        return null;

    }
    protected void onProgressUpdate (String...progress)
    {
        Log.d("ANDRO_ASYNC", progress[0]);
    }

    @Override
    protected void onPostExecute (String unused)
    {
    }
}
