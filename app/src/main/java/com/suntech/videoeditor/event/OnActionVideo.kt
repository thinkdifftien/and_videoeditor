package com.suntech.recorder.event

import com.suntech.recorder.event.OnCallBack
import com.suntech.recorder.model.Media

interface OnActionVideo : OnCallBack {
    fun onResultVideoList(videoList: MutableList<Media>)
    fun deleteSuccess()

}
