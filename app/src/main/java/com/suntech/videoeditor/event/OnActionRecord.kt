package com.suntech.recorder.event

interface OnActionRecord : OnCallBack {
    fun onScreenShotSuccess(imagePath: String)
    fun onScreenShotFailed()
    fun updateCount(count: String)
    fun startRecord()
    fun updateBubbleMenu()
    fun updateBubbleMenuRecorder()
    fun dimBubbleRecord()
    fun updateTimeRecord(time: Int)
    fun updateButtonPause(paused: Boolean)
    fun onRecordFinished(videoPath: String)
}
