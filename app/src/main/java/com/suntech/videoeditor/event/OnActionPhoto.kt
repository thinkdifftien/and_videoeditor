package com.suntech.recorder.event

import com.suntech.recorder.model.Media

interface OnActionPhoto : OnCallBack {
    fun onResultPhoto(images: MutableList<Media>)
    fun renameSuccess()
    fun deleteSuccess()
}
