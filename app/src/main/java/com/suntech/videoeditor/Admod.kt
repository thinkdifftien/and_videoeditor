package com.suntech.videoeditor

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.ads.*
import com.google.android.gms.ads.VideoController.VideoLifecycleCallbacks
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.NativeAdOptions
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.google.android.gms.ads.initialization.InitializationStatus
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener


class Admod {
    private val ID_BANNER_AD = "ca-app-pub-4098909358077926/5622226416"
    private val ID_INTERSTITIAL_AD = "ca-app-pub-4098909358077926/4309144749"
    private val ID_NATIVE_AD = "ca-app-pub-4098909358077926/3786400560"
    private val NUM_SHOW_ADS = 4
    private var numClicked = 0
    private lateinit var mInterstitialAd: InterstitialAd

    init {
        MobileAds.initialize(App.instance, object : OnInitializationCompleteListener {
            override fun onInitializationComplete(initializationStatus: InitializationStatus?) {}
        })
        initInterstitial()
    }

    private fun initInterstitial() {
        mInterstitialAd = InterstitialAd(App.instance)
        mInterstitialAd.setAdUnitId(ID_INTERSTITIAL_AD)
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.setAdListener(object : AdListener() {
            override fun onAdLoaded() {}
            override fun onAdFailedToLoad(errorCode: Int) {}
            override fun onAdOpened() {}
            override fun onAdClicked() {}
            override fun onAdLeftApplication() {}
            override fun onAdClosed() {
                initInterstitial()
            }
        })
    }

    fun loadInterialAds() {
        numClicked++
        if (isLoaded()
            && numClicked > NUM_SHOW_ADS
        ) {
            numClicked = 1
            mInterstitialAd.show()
        }
    }

    private fun isLoaded(): Boolean {
        return mInterstitialAd.isLoaded()
    }

    fun loadNativeAds(context: Context?, frameLayout: FrameLayout) {
        val adView = LayoutInflater.from(context)
            .inflate(R.layout.frame_native_ads, null, false) as UnifiedNativeAdView
        val builder =
            AdLoader.Builder(context, ID_NATIVE_AD)
                .forUnifiedNativeAd { unifiedNativeAd ->
                    populateUnifiedNativeAdView(unifiedNativeAd, adView)
                    frameLayout.addView(adView)
                }
        val videoOptions = VideoOptions.Builder()
            .setStartMuted(true)
            .build()
        val adOptions = NativeAdOptions.Builder()
            .setVideoOptions(videoOptions)
            .build()
        builder.withNativeAdOptions(adOptions)
        val adLoader =
            builder.withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(errorCode: Int) {}
            }).build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateUnifiedNativeAdView(
        unifiedNativeAd: UnifiedNativeAd,
        adView: UnifiedNativeAdView
    ) {
        val mediaView: MediaView =
            adView.findViewById(R.id.ad_media)
        adView.mediaView = mediaView
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        adView.iconView = adView.findViewById(R.id.ad_app_icon)
        (adView.headlineView as TextView).text = unifiedNativeAd.headline


        if (unifiedNativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.GONE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = unifiedNativeAd.callToAction
        }
        if (unifiedNativeAd.icon == null) {
            adView.iconView.visibility = View.GONE
        } else {
            (adView.iconView as ImageView).setImageDrawable(
                unifiedNativeAd.icon.drawable
            )
            adView.iconView.visibility = View.VISIBLE
        }

        adView.setNativeAd(unifiedNativeAd)
        val vc = unifiedNativeAd.videoController
        if (vc.hasVideoContent()) {
            vc.videoLifecycleCallbacks = object : VideoLifecycleCallbacks() {
                override fun onVideoEnd() {
                    super.onVideoEnd()
                }
            }
        } else {
        }
    }

}