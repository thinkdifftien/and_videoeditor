package com.suntech.recorder

import android.content.Intent
import android.media.projection.MediaProjection
import android.media.projection.MediaProjectionManager
import com.suntech.recorder.model.Media

class StorageCommon {
    var isInitedRecorder: Boolean = false
    var mListPhoto: MutableList<Media> = ArrayList()
    var mListVideo: MutableList<Media> = ArrayList()
    var mMediaProjectionRecorder: MediaProjection? = null
    var mMediaProjectionScreenShot: MediaProjection? = null
    var data: Intent? = null

    fun setListPhoto(mListPhoto: MutableList<Media>) {
        this.mListPhoto = mListPhoto
    }

    fun setListVideo(mListVideo: MutableList<Media>) {
        this.mListVideo = mListVideo
    }

    fun setMediaProjection(mMediaProjection: MediaProjection) {
        this.mMediaProjectionRecorder = mMediaProjection
        this.mMediaProjectionScreenShot = mMediaProjection
    }

    fun saveDataIntent(data: Intent) {
        this.data = data
    }

    fun addMedia(media: Media) {
        mListVideo.add(media)
    }

}