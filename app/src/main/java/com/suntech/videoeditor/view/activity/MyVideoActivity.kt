package com.suntech.videoeditor.view.activity

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.baoyz.widget.PullRefreshLayout
import com.suntech.recorder.event.OnActionVideo
import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Video
import com.suntech.recorder.presenter.VideoPresenter
import com.suntech.recorder.utils.CommonUtils
import com.suntech.recorder.view.adapter.VideoAdapter
import com.suntech.recorder.view.base.BaseActivity
import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.videoeditor.App
import com.suntech.videoeditor.R
import com.zaaach.toprightmenu.TopRightMenu

class MyVideoActivity : BaseActivity<VideoPresenter>(), OnActionVideo, PullRefreshLayout.OnRefreshListener, OnActionCallBack, View.OnClickListener  {
    private var frAds: FrameLayout? = null
    private lateinit var newName: String
    private lateinit var video: Video
    private lateinit var videoAdapter: VideoAdapter
    private var mList: MutableList<Media> = ArrayList()
    private var rvVideo: RecyclerView? = null
    private lateinit var iv_back : ImageView
    private var refreshLayout: PullRefreshLayout? = null

    override fun initPresenter() {
       mPresnter = VideoPresenter(this)
    }

    override fun initView() {
        findViewById<ImageView>(R.id.iv_back).setOnClickListener(this)
        refreshLayout =findViewById<PullRefreshLayout>(R.id.refesh_layout)
        frAds = findViewById<FrameLayout>(R.id.fr_ads)
        findViewById<ImageView>(R.id.iv_sort).setOnClickListener(this)
        refreshLayout?.setOnRefreshListener(this)
        rvVideo = findViewById<RecyclerView>(R.id.rv_video)
//        App.admod.loadNativeAds(this, frAds!!)
        showData()
    }

    private fun showData() {
        mList = App.storageCommon.mListVideo
        initRecyclerView()
        if (mList.size != 0) {
            return
        }
        mPresnter?.getRecordVideoList()
    }

    override fun onMenuItemClick(position: Int) {
        when (position) {
            0 -> {
                val intent = Intent(this, EditActivity::class.java)
                intent.putExtra("img_path",video.path)
                Log.v("link",video.path)
                startActivity(intent)
            }
            1 -> {
                CommonUtils.instance.shareFile(this, video.path)
            }
            2 -> {
                mPresnter?.deleteVideo(video)
            }
        }
    }

    private fun initRecyclerView() {
        rvVideo?.layoutManager = GridLayoutManager(this,2)
        videoAdapter = VideoAdapter(mList, this)
        videoAdapter.mCallBack = this
        rvVideo?.adapter = videoAdapter
    }

    override fun getLayoutId(): Int {
       return R.layout.activity_my_video
    }

    override fun onResultVideoList(videoList: MutableList<Media>) {
        mList = videoList
        videoAdapter.updateList(mList)
        App.storageCommon.setListVideo(videoList)
    }


    override fun deleteSuccess() {
        mList.remove(video)
        onResultVideoList(mList)
    }

    override fun onRefresh() {

    }

    override fun callBack(key: String, vararg obj: Any?) {
        if (key.equals(VideoAdapter.KEY_MENU_VIDEO)) {
            var v = obj[0] as View
            video = obj[1] as Video
            getPopupMenuVideo().showAsDropDown(v, -250, 0)
        } else if (key.equals(VideoAdapter.KEY_SHOW_VIDEO)) {
            video = obj[0] as Video
            showVideo(video)
        }
    }


    private fun showVideo(video: Video) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setDataAndType(Uri.parse(video.path), "video/*")
        startActivity(intent)
    }

    override fun onClick(v: View?) {
        if (v!!.id == R.id.iv_sort) {
            var menu = getPopupMenuSort()
            menu.showAsDropDown(v, -380, 0)
            menu.setOnMenuItemClickListener(TopRightMenu.OnMenuItemClickListener { position: Int ->
                when (position) {
                    0 -> {
                        sortByDate()
                    }
                    1 -> {
                        sortByCapacity()
                    }
                }
            })
        }
        if (v!!.id == R.id.iv_back) {
            finish()
        }
    }

    private fun sortByCapacity() {
        mPresnter?.sortByCapacity()
    }

    private fun sortByDate() {
        mPresnter?.sortByDate()
    }


}
