package com.suntech.videoeditor.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.suntech.videoeditor.R
import com.suntech.videoeditor.model.Detailmage
import com.suntech.videoeditor.model.SquareMainCover

class PreviewAdapter (detailSticker: MutableList<Detailmage>, val context: Context) :
    RecyclerView.Adapter<PreviewAdapter.MyPostViewHolder>() {





    private var detailSticker = detailSticker
    private val mContext: Context? = null
    private var mainCover: SquareMainCover? = null
    private val isDownload = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPostViewHolder {
        return MyPostViewHolder(LayoutInflater.from(context).inflate(R.layout.row_download_sticker, parent, false))
    }

    override fun getItemCount(): Int {
        return detailSticker.size
    }

    override fun onBindViewHolder(holder: MyPostViewHolder, position: Int) {
        var item = detailSticker.get(position)
        Glide.with(context)
            .load(item.getUrl())
            .into(holder.tvSticker)
    }

    class MyPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvSticker: ImageView = itemView.findViewById(R.id.imgPreview)

    }
}