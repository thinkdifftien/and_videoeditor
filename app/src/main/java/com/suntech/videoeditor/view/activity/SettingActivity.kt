package com.suntech.videoeditor.view.activity

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import com.suntech.recorder.event.OnActionSetting
import com.suntech.recorder.presenter.SettingPresenter
import com.suntech.recorder.utils.CommonUtils
import com.suntech.recorder.utils.SettingUtils
import com.suntech.recorder.view.base.BaseActivity
import com.suntech.recorder.view.dialog.SettingDialog
import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.videoeditor.R

class SettingActivity : BaseActivity<SettingPresenter>(), OnActionSetting, View.OnClickListener, OnActionCallBack {

    private var tvResolution: TextView? = null
    private var tvQuality: TextView? = null
    private var tvSpeedRecord: TextView? = null
    private var tvCountDownValue: TextView? = null
    private lateinit var swSound: Switch
    private lateinit var swCamera: Switch
    private lateinit var swRecordTime: Switch
    private lateinit var swCountDown: Switch
    private lateinit var btnNext : ImageView
    private lateinit var imgIntro : ImageView
    private  var position : Int = 1

    private var arrBitRate: IntArray = intArrayOf(51000000, 9000000, 6000000, 2000000, 1000000)
    private var arrQuality: IntArray = intArrayOf(2160, 1080, 720, 480, 360)
    private var arrRecordSpeed: IntArray = intArrayOf(30, 60)

    override fun initPresenter() {
        mPresnter = SettingPresenter(this)
    }

    override fun initView() {
        addView()
        updateUI()
        addEvent()
    }

    override fun getLayoutId(): Int {
      return R.layout.frg_setting
    }

    private fun addView() {
        tvResolution = findViewById<TextView>(R.id.tv_resolution)
        tvQuality =findViewById<TextView>(R.id.tv_quality)
        tvSpeedRecord = findViewById<TextView>(R.id.tv_speed_record)
    }

    private fun updateUI() {
        tvResolution?.text =
            "${SettingUtils.instance.getResolution()[0]} x ${SettingUtils.instance.getResolution()[1]}"
        tvQuality?.text =
            "${arrQuality[findPosQuality(arrBitRate, SettingUtils.instance.getQuality())]} P"
        tvSpeedRecord?.text = "${SettingUtils.instance.getRecordSpeed()} FPS"
        tvCountDownValue?.text = "${SettingUtils.instance.getCountDownValue()} Seconds"

    }

    private fun findPosQuality(arrQuality: IntArray, quality: Int): Int {
        for (i in 0..arrQuality.size - 1) {
            if (arrQuality[i] == quality) {
                return i
            }
        }
        return arrQuality.size - 1
    }

    private fun addEvent() {
        findViewById<LinearLayout>(R.id.ln_resolution).setOnClickListener(this)
        findViewById<ImageView>(R.id.btnBack).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_quality).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_record_speed).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_camera).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_policy).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_rate_app).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_share_app).setOnClickListener(this)
        findViewById<LinearLayout>(R.id.ln_about).setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ln_resolution -> {
                pickResolution()
            }
            R.id.btnBack -> {
                finish()
            }
            R.id.ln_quality -> {
                pickQuality()
            }
            R.id.ln_record_speed -> {
                pickRecordSpeed()
            }
            R.id.ln_policy -> {
                CommonUtils.instance.policy(this,0)
            }
            R.id.ln_rate_app -> {
                CommonUtils.instance.rateApp(this)
            }
            R.id.ln_share_app -> {
                CommonUtils.instance.shareApp(this)
            }
            R.id.ln_about -> {
                CommonUtils.instance.policy(this,1)
            }
        }
    }

    private fun changeCountDownBeforeStart() {
        val isCountDownBeforeStart = SettingUtils.instance.isCountDownBeforeStart()
        if (isCountDownBeforeStart) {
            SettingUtils.instance.setCountDownBeforeStart(false)
        } else {
            SettingUtils.instance.setCountDownBeforeStart(true)
        }
        updateUI()
    }

    private fun changeRecordTime() {
        val isRecordTime = SettingUtils.instance.isRecordTime()
        if (isRecordTime) {
            SettingUtils.instance.setRecordTime(false)
        } else {
            SettingUtils.instance.setRecordTime(true)
        }
        updateUI()
    }

    private fun changeCamera() {
        val isCamera = SettingUtils.instance.isCamera()
        if (isCamera) {
            SettingUtils.instance.setCamera(false)
        } else {
            SettingUtils.instance.setCamera(true)
        }
        updateUI()
    }

//    private fun showDialogIntro() {
//        val dialog = Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
//        dialog.setContentView(R.layout.dialog_intro)
//        dialog.show()
//        btnNext = dialog.findViewById(R.id.btnNext)
//        imgIntro = dialog.findViewById(R.id.imgIntro)
//        btnNext.setOnClickListener {
//
//            if(position==1){
//                imgIntro.setBackgroundResource(R.drawable.intro2)
//                position =2
//
//            }
//            else if (position==2){
//                imgIntro.setBackgroundResource(R.drawable.intro3)
//                position =3
//            }
//            else if (position==3){
//                dialog.dismiss()
//            }
//        }
//    }

    private fun changeSound() {
        val isSound = SettingUtils.instance.isSound()
        if (isSound) {
            SettingUtils.instance.setSounds(false)
        } else {
            SettingUtils.instance.setSounds(true)
        }
        updateUI()
    }

    private val PICK_COUT_DOWN_TIME: String = "PICK_COUT_DOWN_TIME"
    private val PICK_QUALITY: String = "PICK_QUALITY"
    private val PICK_RECORD_SPEED: String = "PICK_RECORD_SPEED"
    private val PICK_RESOLUTION: String = "PICK_RESOLUTION"
    private var pickItem: String = ""



    private fun pickRecordSpeed() {
        pickItem = PICK_RECORD_SPEED
        var arrData: MutableList<String> = ArrayList()
        for (i in 0..arrRecordSpeed.size - 1) {
            arrData.add("${arrRecordSpeed[i]}FPS")
        }
        showDialog(arrData)
    }

    private fun pickQuality() {
        pickItem = PICK_QUALITY
        var arrData: MutableList<String> = ArrayList()
        for (i in 0..arrQuality.size - 1) {
            arrData.add("${arrQuality[i]}P")
        }
        showDialog(arrData)
    }

    private fun pickResolution() {
        pickItem = PICK_RESOLUTION
        var arrData: MutableList<String> = ArrayList()
        arrData.add("1920x1080")
        arrData.add("1280x720")
        arrData.add("854x480")
        arrData.add("640x360")
        showDialog(arrData)
    }

    fun showDialog(data: MutableList<String>) {
        var settingDialog = SettingDialog(this)
        settingDialog.setData(data)
        settingDialog.mCallBack = this
        settingDialog.show()
    }

    override fun callBack(key: String, vararg obj: Any?) {
        val data = obj[0].toString()
        when (pickItem) {
            PICK_COUT_DOWN_TIME -> {
                val time = data.trim().toInt()
                SettingUtils.instance.setCountDownValue(time)
            }
            PICK_QUALITY -> {
                val quality = data.replace("P", "").trim().toInt()
                SettingUtils.instance.setQuality(arrBitRate[findPosQuality(arrQuality, quality)])
            }
            PICK_RECORD_SPEED -> {
                val recordSpeed = data.replace("FPS", "").trim().toInt()
                SettingUtils.instance.setRecordSpeed(recordSpeed)
            }
            PICK_RESOLUTION -> {
                val arrInt: List<String> = data.split("x")
                val width = arrInt.get(0).trim().toInt()
                val height = arrInt.get(1).trim().toInt()
                val resolution = intArrayOf(width, height)
                SettingUtils.instance.setResolution(resolution)
            }
        }
        updateUI()
    }


}
