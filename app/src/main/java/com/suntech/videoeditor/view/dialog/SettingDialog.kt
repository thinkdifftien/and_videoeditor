package com.suntech.recorder.view.dialog

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager

import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.recorder.view.adapter.SettingAdapter
import com.suntech.recorder.view.base.BaseDialog
import com.suntech.videoeditor.R
import kotlinx.android.synthetic.main.dialog_setting.*

class SettingDialog(ctx: Context) : BaseDialog<Any>(ctx),
    OnActionCallBack {
    private var arrData: MutableList<String> = ArrayList()
    private lateinit var title: String

    override fun initPresenter() {
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_setting
    }

    override fun initView() {
        setTransparentBG()
        initRecyclerView()
    }

    private fun initRecyclerView() {
        rc_setting.layoutManager = LinearLayoutManager(context)
        var settingAdapter = SettingAdapter(arrData, context)
        settingAdapter.mCallBack = this
        rc_setting.adapter = settingAdapter
    }

    fun setTitle(title: String) {
        this.title = this.title
    }

    fun setData(arrData: MutableList<String>) {
        this.arrData = arrData
    }

    override fun callBack(key: String, vararg obj: Any?) {
        mCallBack.callBack(key, obj[0])
        dismiss()
    }
}