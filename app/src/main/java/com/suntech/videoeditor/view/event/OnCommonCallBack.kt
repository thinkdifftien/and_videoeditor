package com.suntech.recorder.view.event

import android.view.animation.Animation

abstract class OnCommonCallBack : Animation.AnimationListener{
    override fun onAnimationRepeat(animation: Animation?) {
    }

    override fun onAnimationEnd(animation: Animation?) {
    }

    override fun onAnimationStart(animation: Animation?) {
    }

}