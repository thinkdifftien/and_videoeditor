package com.suntech.videoeditor.view.activity;

import android.os.Bundle;

import com.suntech.videoeditor.R;
import com.suntech.videoeditor.view.base.BaseCameraActivity;

public class CameraActivity extends BaseCameraActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        onCreateActivity();
        videoWidth = 720;
        videoHeight = 1280;
        cameraWidth = 1280;
        cameraHeight = 720;
    }
}
