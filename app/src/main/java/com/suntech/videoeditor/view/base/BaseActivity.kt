package com.suntech.recorder.view.base

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.DisplayMetrics
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import com.suntech.recorder.utils.CommonUtils
import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.videoeditor.App
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.activity.MainActivity
import com.zaaach.toprightmenu.MenuItem
import com.zaaach.toprightmenu.TopRightMenu
import java.util.ArrayList

abstract class BaseActivity<T> : AppCompatActivity(), OnActionCallBack ,TopRightMenu.OnMenuItemClickListener {
    protected var mHeightScreen: Int = 0
    protected var mWidthScreen: Int = 0
    protected val displayMetrics = DisplayMetrics()
    protected var mDisplay: Display? = null
    protected var mDensity: Int = 0

    protected var mPresnter: T? = null

    private var preTime: Long = 0
    private val TIME_DELAY_EXIT_APP: Long = 2000

    private var mHandler: Handler = Handler(Handler.Callback { msg: Message ->
        excuteHandler(msg)
        return@Callback false
    })

    open protected fun excuteHandler(msg: Message) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        customUI()
        try {
            setContentView(getLayoutId())
        } catch (e: Exception) {
        }
        addSizeView()
        initPresenter()
        initView()
    }

    @SuppressLint("SourceLockedOrientationActivity")
    private fun customUI() {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mDisplay = windowManager.defaultDisplay
        mDisplay?.getMetrics(displayMetrics)
        mDensity = displayMetrics.densityDpi
    }

    open fun fullScreencall() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            val v = this.window.decorView
            v.systemUiVisibility = View.GONE
        } else if (Build.VERSION.SDK_INT >= 19) { //for new api versions.
            val decorView = window.decorView
            val uiOptions =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            decorView.systemUiVisibility = uiOptions
        }
    }

    private fun addSizeView() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        mHeightScreen = displayMetrics.heightPixels
        mWidthScreen = displayMetrics.widthPixels
    }

    abstract fun initPresenter()

    protected abstract fun initView()

    protected abstract fun getLayoutId(): Int

    protected fun backInMain() {
        val currentTime = System.currentTimeMillis()
        if (currentTime - preTime > TIME_DELAY_EXIT_APP) {
            CommonUtils.instance.toast(
                this.getString(R.string.txt_press_back_to_exit)
            )
            preTime = currentTime
        } else {
            moveTaskToBack(true)
        }
    }

    protected fun <T : View?> findViewById(id: Int, event: View.OnClickListener?): T {
        val v: T = findViewById<T>(id)
        if (event != null) {
            v!!.setOnClickListener(event)
        }
        return v
    }



    fun getPopupMenuPhoto(): TopRightMenu {
        var menuItems = ArrayList<MenuItem>()
        menuItems.add(MenuItem(R.drawable.ic_edit_small, App.instance.getString(R.string.txt_edit)))
        menuItems.add(
            MenuItem(
                R.drawable.ic_share_small,
                App.instance.getString(R.string.txt_share_image)
            )
        )
        menuItems.add(
            MenuItem(
                R.drawable.ic_delete_small,
                App.instance.getString(R.string.txt_delete)
            )
        )
        var mPopupMenu = TopRightMenu(this)
        mPopupMenu.setHeight(600)
            .setWidth(400)
            .showIcon(true)
            .dimBackground(true)
            .setOnMenuItemClickListener(this)

            .addMenuList(menuItems)
        return mPopupMenu
    }

    fun getPopupMenuSort(): TopRightMenu {
        var menuItems = ArrayList<MenuItem>()
        menuItems.add(
            MenuItem(
                R.drawable.ic_duration_small,
                App.instance.getString(R.string.txt_sort_by_time)
            )
        )
        menuItems.add(
            MenuItem(
                R.drawable.ic_size_media_small,
                App.instance.getString(R.string.txt_sort_by_capacity)
            )
        )

        var mPopupMenu = TopRightMenu(this)
        mPopupMenu
            .showIcon(true)
            .dimBackground(true)
            .setOnMenuItemClickListener(this)
            .addMenuList(menuItems)
        return mPopupMenu
    }

    fun getPopupMenuVideo(): TopRightMenu {
        var menuItems = ArrayList<MenuItem>()
        menuItems.add(
            MenuItem(
                R.drawable.ic_edit_small,
                App.instance.getString(R.string.txt_edit)
            )
        )
        menuItems.add(
            MenuItem(
                R.drawable.ic_share_small,
                App.instance.getString(R.string.txt_share_image)
            )
        )
        menuItems.add(
            MenuItem(
                R.drawable.ic_delete_small,
                App.instance.getString(R.string.txt_delete)
            )
        )
        var mPopupMenu = TopRightMenu(this)
        mPopupMenu.setHeight(420)
            .setWidth(350)
            .showIcon(true)
            .dimBackground(true)
            .setOnMenuItemClickListener(this)
            .addMenuList(menuItems)
        return mPopupMenu
    }

    protected open fun updateUI(tag: String?) {

    }

    override fun callBack(key: String, vararg obj: Any?) {

    }

    override fun onMenuItemClick(position: Int) {

    }

    protected fun updateHandler(what: Int, arg1: Int = 0, arg2: Int = 0) {
        val msg = Message()
        msg.target = mHandler
        msg.what = what
        msg.arg1 = arg1
        msg.arg2 = arg2
        msg.sendToTarget()
    }

    protected fun backHome() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

}