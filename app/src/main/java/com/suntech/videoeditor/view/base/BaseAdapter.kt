package com.suntech.recorder.view.base

import androidx.recyclerview.widget.RecyclerView
import com.suntech.recorder.view.event.OnActionCallBack

abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
     lateinit var mCallBack: OnActionCallBack

}

