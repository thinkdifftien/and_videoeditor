package com.suntech.recorder.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Video
import com.suntech.recorder.utils.CommonUtils
import com.suntech.recorder.view.base.BaseAdapter
import com.suntech.recorder.view.base.BaseViewHolder
import com.suntech.videoeditor.R

class VideoAdapter(mList: MutableList<Media>, context: Context) : BaseAdapter() {
    companion object {
        val KEY_MENU_VIDEO: String = "KEY_MENU_VIDEO"
        val KEY_SHOW_VIDEO: String = "KEY_SHOW_VIDEO"
    }

    private var mList = mList
    private var context = context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var v = LayoutInflater.from(context).inflate(R.layout.item_video, parent, false)
        return VideoViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var mholder: VideoViewHolder = holder as VideoViewHolder
        var video = mList.get(position) as Video
        mholder.ivPhoto.setImageBitmap(video.thumbnail)
        mholder.tvName.text = video.name
        mholder.tvDuration.text = CommonUtils.instance.formatTime(video.duration)
        mholder.tvSize.text = CommonUtils.instance.formatSize(video.size)
        mholder.ivMenuMedia.setOnClickListener(View.OnClickListener {
            mCallBack.callBack(KEY_MENU_VIDEO, mholder.ivMenuMedia, video)
        })
        mholder.itemView.setOnClickListener(View.OnClickListener {
            mCallBack.callBack(KEY_SHOW_VIDEO, video)
        })
    }

    fun updateList(mList: MutableList<Media>) {
        this.mList = mList
        notifyDataSetChanged()
    }

    class VideoViewHolder(itemView: View) : BaseViewHolder(itemView) {
        var ivPhoto = itemView.findViewById<ImageView>(R.id.iv_photo)
        var ivMenuMedia =
            itemView.findViewById<ImageView>(R.id.iv_menu_media)
        var tvName = itemView.findViewById<TextView>(R.id.tv_name)
        var tvDuration = itemView.findViewById<TextView>(R.id.tv_duration)
        var tvSize = itemView.findViewById<TextView>(R.id.tv_size)

    }
}
