package com.suntech.recorder.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Photo
import com.suntech.recorder.view.base.BaseAdapter
import com.suntech.recorder.view.base.BaseViewHolder
import com.suntech.videoeditor.R

class PhotoAdapter(mList: MutableList<Media>, mContext: Context) : BaseAdapter() {
    companion object {
        val KEY_MENU_PHOTO: String = "KEY_MENU_PHOTO"
        val KEY_SHOW_IMAGE: String = "SHOW_IMAGE"
        val KEY_EDIT: String = "EDIT_IMAGE"
    }

    private var mList = mList
    private var mContext = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(mContext).inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var mHolder = holder as PhotoViewHolder
        var photo = mList.get(position) as Photo
        mHolder.tvName.text = photo.name
        Glide.with(mContext).load(photo.path).into(mHolder.ivPhoto)
        mHolder.ivMenuPhoto.setOnClickListener(View.OnClickListener {
            mCallBack.callBack(KEY_MENU_PHOTO, mHolder.ivMenuPhoto, photo)
        })
        mHolder.itemView.setOnClickListener(View.OnClickListener {
            mCallBack.callBack(KEY_SHOW_IMAGE, photo)
        })
    }

    fun updateList(images: MutableList<Media>) {
        this.mList = images
        notifyDataSetChanged()
    }

    class PhotoViewHolder(itemView: View) : BaseViewHolder(itemView) {
        var ivPhoto = itemView.findViewById<ImageView>(R.id.iv_photo)
        var ivMenuPhoto =
            itemView.findViewById<ImageView>(R.id.iv_menu_media)
        var tvName = itemView.findViewById<TextView>(R.id.tv_name)

    }
}