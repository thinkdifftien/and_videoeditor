package com.suntech.videoeditor.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.hw.photomovie.render.GLTextureView
import com.suntech.recorder.utils.CommonUtils
import com.suntech.videoeditor.R
import com.suntech.videoeditor.interfaces.ISlideView
import com.suntech.videoeditor.presenter.SlidePresenter
import com.suntech.videoeditor.widget.*
import me.iwf.photopicker.PhotoPicker

class SlideActivity : AppCompatActivity(), View.OnClickListener, ISlideView, MovieBottomView.MovieBottomCallback {

    private val REQUEST_MUSIC = 234
    private lateinit var mSelectView : LinearLayout
    private  var mTransferView : MovieTransferView ?=null
    private  var mFilterView : MovieFilterView ?=null
    private lateinit var mGLTextureView: GLTextureView
    private lateinit var mBottomView: MovieBottomView
    private var mSlidePrenter =  SlidePresenter()
    private var mTransfers: List<TransferItem>? = null
    private var mFilters: List<FilterItem>? = null
    private lateinit var imtBack : ImageView

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.movie_add ->{
                PhotoPicker.builder()
                    .setPhotoCount(9)
                    .setShowCamera(false)
                    .setShowGif(false)
                    .setPreviewEnabled(true)
                    .start(this, PhotoPicker.REQUEST_CODE)
            }
            R.id.btnBack -> {
                finish()
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_slide)
        initView()
    }

    private fun initView() {
        mBottomView = findViewById(R.id.movie_bottom_layout)
        findViewById<ImageView>(R.id.btnBack).setOnClickListener(this)
        mBottomView.setCallback(this)
        mSelectView = findViewById(R.id.movie_add)
        mSelectView.setOnClickListener(this)
        mGLTextureView = findViewById(R.id.gl_texture)
        mSlidePrenter.attachView(this)
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == REQUEST_MUSIC) {
            val uri = data!!.data
            mSlidePrenter.setMusic(uri)
        }
        else if (resultCode == RESULT_OK && requestCode == PhotoPicker.REQUEST_CODE) {
            if (data != null) {
                val photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS)
                mSlidePrenter.onPhotoPick(photos)
                mSelectView.setVisibility(View.GONE)
            }
        }
    }

    override fun setFilters(filters: MutableList<FilterItem>?) {
        mFilters = filters
    }

    override fun getActivity(): Activity {
        return  this
    }

    override fun setTransfers(items: MutableList<TransferItem>?) {
        mTransfers = items
    }

    override fun getGLView(): GLTextureView {
        return mGLTextureView
    }

    private fun checkInit(): Boolean {
        if (mSelectView.visibility == View.VISIBLE) {
            Toast.makeText(this, "please select photos", Toast.LENGTH_LONG).show()
            return true
        }
        return false
    }

    override fun onTransferClick() {
        if (checkInit()) {
            return
        }
        if (mTransferView == null) {
            val stub: ViewStub = findViewById(R.id.movie_menu_transfer_stub)
            mTransferView = stub.inflate() as MovieTransferView
            mTransferView!!.setVisibility(View.GONE)
            mTransferView!!.setItemList(mTransfers)
            mTransferView!!.setTransferCallback(mSlidePrenter)
        }
        mBottomView.visibility = View.GONE
        mTransferView!!.show()
    }

    override fun onFilterClick() {
        if (checkInit()) {
            return
        }
        if (mFilterView == null) {
            val stub: ViewStub = findViewById(R.id.movie_menu_filter_stub)
            mFilterView = stub.inflate() as MovieFilterView
            mFilterView!!.setVisibility(View.GONE)
            mFilterView!!.setItemList(mFilters)
            mFilterView!!.setFilterCallback(mSlidePrenter)
        }
        mBottomView.visibility = View.GONE
        mFilterView!!.show()
    }

    override fun onMusicClick() {
        if (checkInit()) {
            return
        }
        val i = Intent()
        i.type = "audio/*"
        i.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(i,REQUEST_MUSIC)
    }

    override fun onNextClick() {
        if (checkInit()) {
            return
        }
        mSlidePrenter.saveVideo()
    }

    private fun checkInArea(view: View, event: MotionEvent): Boolean {
        val loc = IntArray(2)
        view.getLocationInWindow(loc)
        return event.rawY > loc[1]
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_DOWN) {
            if (mFilterView != null && mFilterView!!.visibility === View.VISIBLE && !checkInArea(
                    mFilterView!!, ev)) {
                mFilterView!!.hide()
                mBottomView.visibility = View.VISIBLE
                return true
            } else if (mTransferView != null && mTransferView!!.visibility === View.VISIBLE && !checkInArea(mTransferView!!, ev)) {
                mTransferView!!.hide()
                mBottomView.visibility = View.VISIBLE
                return true
            }
        }
        return super.dispatchTouchEvent(ev)
    }


    override fun onPause() {
        super.onPause()
        mSlidePrenter.onPause()
        mGLTextureView.onPause()
    }

    override fun onResume() {
        super.onResume()
        mSlidePrenter.onResume()
        mGLTextureView.onResume()
    }

}
