package com.suntech.recorder.view.event

interface OnActionCallBack {
    fun callBack(key: String, vararg obj: Any?)

}