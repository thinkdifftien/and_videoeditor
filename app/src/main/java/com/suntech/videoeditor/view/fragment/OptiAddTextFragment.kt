/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.suntech.recorder.view.fragment

import android.content.Context
import android.os.Bundle
import android.os.Environment

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.suntech.recorder.OptiVideoEditor
import com.suntech.recorder.interfaces.OptiFFMpegCallback
import com.suntech.recorder.interfaces.OptiPositionListener
import com.suntech.recorder.utils.ConstantUtils
import com.suntech.recorder.utils.OptiUtils
import com.suntech.recorder.view.adapter.OptiPositionAdapter
import com.suntech.videoeditor.R

import java.io.File
import java.util.*

 class OptiAddTextFragment : BottomSheetDialogFragment(), OptiPositionListener, OptiFFMpegCallback{

    private var tagName: String = OptiAddTextFragment::class.java.simpleName
    private lateinit var rootView: View
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var rvPosition: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var videoFile: File? = null
    private var helper: OptiBaseCreatorDialogFragment.CallBacks? = null
    private lateinit var optiPositionAdapter: OptiPositionAdapter
    private var positionList: ArrayList<String> = ArrayList()
    private var selectedPositionItem: String? = null
    private var etText: EditText? = null
    private var positionStr: String? = null
    private var mContext: Context? = null
    private  var dialog : MaterialDialog ?=null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.opti_fragment_add_text, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvPosition = rootView.findViewById(R.id.rvPosition)
        ivClose = rootView.findViewById(R.id.iv_close)
        ivDone = rootView.findViewById(R.id.iv_done)
        etText = rootView.findViewById(R.id.etText)
        linearLayoutManager = LinearLayoutManager(activity!!.applicationContext)

        linearLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
        rvPosition.layoutManager = linearLayoutManager

        mContext = context

        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {

            val text = etText!!.text.toString().trim()
            Log.v(tagName, "userText: $text")

            if (text.isNotEmpty()) {
                optiPositionAdapter.setPosition()

                if (selectedPositionItem != null) {
                    dismiss()

                    //get selected position to apply text on video
                    when (selectedPositionItem) {
                        ConstantUtils.BOTTOM_LEFT -> { //not working
                            positionStr = OptiVideoEditor.POSITION_BOTTOM_LEFT
                        }

                        ConstantUtils.BOTTOM_RIGHT -> {
                            positionStr = OptiVideoEditor.POSITION_BOTTOM_RIGHT
                        }

                        ConstantUtils.CENTRE_ALIGN -> {
                            positionStr = OptiVideoEditor.POSITION_CENTER_ALLIGN
                        }

                        ConstantUtils.CENTRE_BOTTOM -> {
                            positionStr = OptiVideoEditor.POSITION_CENTER_BOTTOM
                        }

                        ConstantUtils.TOP_LEFT -> {
                            positionStr = OptiVideoEditor.POSITION_TOP_LEFT
                        }

                        ConstantUtils.TOP_RIGHT -> {
                            positionStr = OptiVideoEditor.POSITION_TOP_RIGHT
                        }
                    }

                    //output file is generated and send to video processing
                    val outputFile = OptiUtils.createVideoFile(context!!)
                    Log.v(tagName, "outputFile: ${outputFile.absolutePath}")

                    //get font file
                    val fontFile = File(
                        Environment.getExternalStorageDirectory(),
                        File.separator + ConstantUtils.APP_NAME + File.separator + ConstantUtils.FONT + File.separator + ConstantUtils.DEFAULT_FONT
                    )
                    Log.v(tagName, "fontPath: ${fontFile.absolutePath}")

                    OptiVideoEditor.with(context!!)
                        .setType(ConstantUtils.VIDEO_TEXT_OVERLAY)
                        .setFile(videoFile!!)
                        .setOutputPath(outputFile.path)
                        .setFont(fontFile)
                        .setText(text)
                        .setColor("#FFFFFF")
                        .setSize("32")
                        .addBorder(false)
                        .setPosition(positionStr!!)
                        .setCallback(this)
                        .main()

                        showLoading()
                } else {
                    OptiUtils.showGlideToast(activity!!, getString(R.string.error_add_text_pos))
                }
            } else {
                OptiUtils.showGlideToast(activity!!, getString(R.string.error_add_text))
            }
        }

        //positionList.add(OptiConstant.BOTTOM_LEFT)
        positionList.add(ConstantUtils.BOTTOM_RIGHT)
        positionList.add(ConstantUtils.CENTRE_ALIGN)
        positionList.add(ConstantUtils.CENTRE_BOTTOM)
        positionList.add(ConstantUtils.TOP_LEFT)
        positionList.add(ConstantUtils.TOP_RIGHT)

        optiPositionAdapter = OptiPositionAdapter(positionList, activity!!.applicationContext, this)
        rvPosition.adapter = optiPositionAdapter
        optiPositionAdapter.notifyDataSetChanged()
    }

    fun setHelper(helper: OptiBaseCreatorDialogFragment.CallBacks) {
        this.helper = helper
    }

    fun setFilePathFromSource(file: File) {
        videoFile = file
    }

    override fun selectedPosition(position: String) {
        selectedPositionItem = position
    }

    override fun onProgress(progress: String) {
        Log.v(tagName, "onProgress()")
    }

    override fun onSuccess(convertedFile: File, type: String) {
        dialog!!.dismiss()
        Log.v(tagName, "onSuccess()")
        helper?.onFileProcessed(convertedFile)
    }

    override fun onFailure(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onFailure() ${error.localizedMessage}")
        Toast.makeText(mContext, "Video processing failed", Toast.LENGTH_LONG).show()

    }

    override fun onNotAvailable(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onNotAvailable() ${error.localizedMessage}")
    }

    override fun onFinish() {
        Log.v(tagName, "onFinish()")
        dialog!!.dismiss()
    }

    private fun showLoading() {
        dialog = context?.let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }
    }
}