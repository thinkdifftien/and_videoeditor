package com.suntech.videoeditor.view.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.suntech.videoeditor.R
import com.suntech.videoeditor.interfaces.OptiBackgroundListener
import com.suntech.videoeditor.model.Thumbail

class OptiBackgroundAdapter(mList: List<Thumbail>, val context: Context,  optiBackgroundListener: OptiBackgroundListener) :
    RecyclerView.Adapter<OptiBackgroundAdapter.MyPostViewHolder>()  {

    private var mThumbail = mList
    private var selectedPosition: Int = -1
    private var selectedFilePath: String? = null
    private var myBackgroundtListener = optiBackgroundListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPostViewHolder {
        return MyPostViewHolder(LayoutInflater.from(context).inflate(R.layout.opti_background_view, parent, false))
    }

    override fun getItemCount(): Int {
       return mThumbail.size
    }

    override fun onBindViewHolder(holder:MyPostViewHolder, position: Int) {
        var item = mThumbail.get(position)
        val Image: ImageView = holder.tvThumbails
        val url = "http://app-wpt.pocketsnap.netdna-cdn.com/"+item.getThumbnailLow()
        Picasso.get()
            .load(url)
            .resize(150, 320)
            .centerCrop()
            .into(Image)
        if (selectedPosition == position) {
            holder.tvThumbails.setBackgroundColor(Color.WHITE)
        } else {
            holder.tvThumbails.setBackgroundColor(Color.BLACK)
        }

        holder.tvThumbails.setOnClickListener {
            //selected clip art will be saved here
            selectedFilePath = "http://app-wpt.pocketsnap.netdna-cdn.com/"+item.getThumbnail()
            selectedPosition = position
            notifyDataSetChanged()
        }
    }

    fun setBackground() {
        if (selectedFilePath != null) {
            myBackgroundtListener.selectedBackground(selectedFilePath!!)
        }
    }

    class MyPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvThumbails: ImageView = itemView.findViewById(R.id.imgThumbail)
    }
}