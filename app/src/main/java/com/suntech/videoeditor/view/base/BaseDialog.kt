package com.suntech.recorder.view.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.suntech.recorder.view.event.OnActionCallBack

abstract class BaseDialog<T>(ctx: Context) : Dialog(ctx) {
    protected var mPresenter: T? = null
    lateinit var mCallBack: OnActionCallBack

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        initPresenter()
        initView()
    }

    abstract fun initPresenter()

    abstract fun initView()

    abstract fun getLayoutId(): Int

    fun setTransparentBG() {
        getWindow()?.setBackgroundDrawableResource(android.R.color.transparent);
    }

}