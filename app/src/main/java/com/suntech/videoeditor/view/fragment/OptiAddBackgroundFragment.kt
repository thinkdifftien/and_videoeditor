package com.suntech.videoeditor.view.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.suntech.recorder.OptiVideoEditor
import com.suntech.recorder.interfaces.OptiFFMpegCallback
import com.suntech.recorder.utils.ConstantUtils
import com.suntech.recorder.utils.OptiUtils
import com.suntech.recorder.view.fragment.OptiAddClipArtFragment
import com.suntech.recorder.view.fragment.OptiBaseCreatorDialogFragment
import com.suntech.videoeditor.R
import com.suntech.videoeditor.api.API
import com.suntech.videoeditor.interfaces.OptiBackgroundListener
import com.suntech.videoeditor.model.Thumbail
import com.suntech.videoeditor.view.adapter.OptiBackgroundAdapter
import com.suntech.videoeditor.widget.DownloadBackground
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class OptiAddBackgroundFragment: BottomSheetDialogFragment(), OptiBackgroundListener,OptiFFMpegCallback, Callback<List<Thumbail>> {

    private var mContext: Context? = null
    private lateinit var rootView: View
    private var helper: OptiBaseCreatorDialogFragment.CallBacks? = null
    private var videoFile: File? = null
    private var tagName: String = OptiAddClipArtFragment::class.java.simpleName
    var thumbails: MutableList<Thumbail> = mutableListOf()
    private lateinit var optiBackgroundAdapter: OptiBackgroundAdapter
    private lateinit var rvBackground: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private lateinit var linearLayoutManagerOne: LinearLayoutManager
    private var selectedFilePath: String? = null
    private  var dialog : MaterialDialog ?=null
    private var outputFile : File? =null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.opti_fragment_background, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvBackground = rootView.findViewById(R.id.rvBackground)

        ivClose = rootView.findViewById(R.id.iv_close)
        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone = rootView.findViewById(R.id.iv_done)
        ivDone.setOnClickListener {
            optiBackgroundAdapter.setBackground()

            if (selectedFilePath != null) {
                Log.v("file_path", selectedFilePath)
                    showLoading()
                    dismiss()
                    addBackgroundAction(selectedFilePath!!,OptiVideoEditor.CENTER_ALLIGN)

            } else {
                OptiUtils.showGlideToast(activity!!, getString(R.string.error_select_sticker))
            }
        }

        linearLayoutManagerOne = LinearLayoutManager(activity!!.applicationContext)

        linearLayoutManagerOne.orientation = LinearLayoutManager.HORIZONTAL
        rvBackground.layoutManager = linearLayoutManagerOne

        optiBackgroundAdapter = OptiBackgroundAdapter(thumbails, activity!!.applicationContext,this)
        rvBackground.adapter = optiBackgroundAdapter
        optiBackgroundAdapter.notifyDataSetChanged()
        mContext = context
        API.apiThumbail.thumbail.enqueue(this)

    }

    private fun addBackgroundAction(imgPath: String, position: String) {
        //output file is generated and it is send to video processing
        outputFile = OptiUtils.createVideoFile(context!!)
        Log.v(tagName, "outputFile: ${outputFile!!.absolutePath}")

        OptiVideoEditor.with(context!!)
            .setType(ConstantUtils.VIDEO_BACKGROUND_OVERLAY)
            .setFile(videoFile!!)
            .setOutputPath(outputFile!!.path)
            .setImagePath("/storage/emulated/0/VideoEditor/background.png")
            .setPosition(position)
            .setCallback(this)
            .main()
    }

    fun setHelper(helper: OptiBaseCreatorDialogFragment.CallBacks) {
        this.helper = helper
    }

    fun setFilePathFromSource(file: File) {
        videoFile = file
        Log.v(tagName, file.absolutePath.toString())
    }

    override fun selectedBackground(path: String) {
        selectedFilePath = path
        val asaytask = DownloadBackground(activity)
        asaytask.execute(selectedFilePath)
    }

    override fun onProgress(progress: String) {
        Log.v(tagName, "onProgress()")
    }

    override fun onSuccess(convertedFile: File, type: String) {
        Log.v(tagName, "onSuccess()")
        dialog!!.dismiss()
        helper?.onFileProcessed(convertedFile)
    }

    override fun onFailure(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onFailure() ${error.localizedMessage}")
        Toast.makeText(mContext, "Video processing failed", Toast.LENGTH_LONG).show()
    }

    override fun onNotAvailable(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onNotAvailable() ${error.localizedMessage}")
    }

    override fun onFinish() {
        dialog!!.dismiss()
    }

    override fun onFailure(call: Call<List<Thumbail>>, t: Throwable) {

    }

    override fun onResponse(call: Call<List<Thumbail>>, response: Response<List<Thumbail>>) {
        if(response==null|| response.body()==null){
            return
        }
        thumbails.addAll(response.body()!!)
        optiBackgroundAdapter.notifyDataSetChanged()
    }

    private fun showLoading() {
        dialog = context?.let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }
    }
}