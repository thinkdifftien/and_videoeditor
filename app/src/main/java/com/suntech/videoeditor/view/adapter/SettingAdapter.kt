package com.suntech.recorder.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.suntech.recorder.view.base.BaseAdapter
import com.suntech.recorder.view.base.BaseViewHolder
import com.suntech.videoeditor.R

class SettingAdapter(arrData: MutableList<String>, mContext: Context) : BaseAdapter() {
    companion object {
        val KEY_MENU_PHOTO: String = "KEY_MENU_PHOTO"
    }

    private var mList = arrData
    private var mContext = mContext

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(mContext).inflate(R.layout.item_setting, parent, false)
        return SettingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var mHolder = holder as SettingViewHolder
        var value = mList.get(position)
        mHolder.tvItemSetting.text = value
        mHolder.itemView.setOnClickListener(View.OnClickListener {
            mCallBack.callBack("", value)
        })

    }

    class SettingViewHolder(itemView: View) : BaseViewHolder(itemView) {
        var tvItemSetting = itemView.findViewById<TextView>(R.id.tv_item_setting)

    }
}