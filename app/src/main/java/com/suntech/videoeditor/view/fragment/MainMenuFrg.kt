package com.suntech.recorder.view.fragment


import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.suntech.recorder.model.Menu
import com.suntech.recorder.view.adapter.MenuAdapter
import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.activity.EditActivity
import java.io.*

class MainMenuFrg : Fragment(), OnActionCallBack {

    private lateinit var photo: Menu
    private lateinit var photoAdapter: MenuAdapter
    private lateinit var mList: MutableList<Menu>
    private var rc_menu: RecyclerView? = null
    val list_menu = arrayOf("Gif", "Trim", "Filter","Speed","Crop", "Audio", "Rotate","Icon","Background")


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v: View = inflater.inflate(R.layout.frg_menu, container, false)
        initView(v)
        return v
    }

    private fun initView(v: View) {
        rc_menu = v.findViewById<RecyclerView>(R.id.rv_menu)
        mList = ArrayList()
        showData();
    }

    private fun showData() {
        val assetManager = context!!.assets
        try
        {
            var imgPath: Array<String>? = assetManager.list("menu")
            for(i in imgPath!!.indices)
            {
                var inputstream: InputStream = assetManager.open("menu/" +"menu" +(i) + ".png")

                var bitmap: Bitmap = BitmapFactory.decodeStream(inputstream)
                photo = Menu(bitmap,list_menu.get(i))
                mList.add(photo)
            }
            rc_menu?.layoutManager = LinearLayoutManager(context,RecyclerView.HORIZONTAL,false)
            photoAdapter = MenuAdapter(mList, context!!)
            photoAdapter.mCallBack = this
            rc_menu?.adapter = photoAdapter
        }
        catch (e: IOException)
        {
            Toast.makeText(activity, e.printStackTrace().toString(),Toast.LENGTH_SHORT).show()

        }
    }

    override fun callBack(key: String, vararg obj: Any?) {
        val v = obj[0] as Int
        (activity as EditActivity).getMenuFragment(v)
    }
}

