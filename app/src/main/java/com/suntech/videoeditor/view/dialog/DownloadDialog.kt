package com.suntech.videoeditor.view.dialog

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.suntech.recorder.view.dialog.GIFDialog
import com.suntech.videoeditor.R
import pl.droidsonroids.gif.GifDrawable
import pl.droidsonroids.gif.GifImageView
import java.io.File

class DownloadDialog : DialogFragment() {

    companion object {
        val TAG = DownloadDialog::javaClass.name

        lateinit var file: File

        fun show(fragmentManager: FragmentManager, file: File) {
            this.file = file
            DownloadDialog().show(fragmentManager, TAG)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = activity!!.layoutInflater.inflate(R.layout.dialog_preview, null)

        val tvDownload = view.findViewById<GifImageView>(R.id.tvComplete)
        val tvCancle = view.findViewById<GifImageView>(R.id.tvCancel)

        tvDownload.setOnClickListener {

        }

        return AlertDialog.Builder(activity)
            .setView(view)
            .setTitle("Preview")
            .create()
    }
}
