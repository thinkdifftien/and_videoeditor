/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.suntech.recorder.view.adapter

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.suntech.recorder.interfaces.OptiClipArtListener
import com.suntech.videoeditor.R

class OptiClipArtAdapter(clipArtList: ArrayList<String>, val context: Context,val type:String, optiClipArtListener: OptiClipArtListener) :
    RecyclerView.Adapter<OptiClipArtAdapter.MyPostViewHolder>() {

    private var tagName: String = OptiClipArtAdapter::class.java.simpleName
    private var myClipArtList = clipArtList
    private var myClipArtListener = optiClipArtListener
    private var selectedPosition: Int = -1
    private var selectedFilePath: String? = null
    var imageOption: DisplayImageOptions = DisplayImageOptions.Builder()
        .cacheInMemory(true)
        .showImageOnLoading(R.drawable.yd_image_tx)
        .build()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyPostViewHolder {
        return MyPostViewHolder(LayoutInflater.from(context).inflate(R.layout.opti_clipart_view, p0, false))
    }

    override fun getItemCount(): Int {
        return myClipArtList.size
    }

    override fun onBindViewHolder(holder: MyPostViewHolder, position: Int) {

        val path: String = myClipArtList.get(position)
        if (type == "TYPE_ASSETS") {
            ImageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context))
            ImageLoader.getInstance().displayImage("assets://$path", holder.tvClipArt, imageOption)
            val layoutParams = LinearLayout.LayoutParams(200, 200)
            Log.d("huhu","assets://$path")
            holder.tvClipArt.setLayoutParams(layoutParams)
        } else if (type === "TYPE_DOWNLOAD") {
            val myBitmap = BitmapFactory.decodeFile(path)
            holder.tvClipArt.setImageBitmap(myBitmap)
        }

        if (selectedPosition == position) {
            holder.tvClipArt.setBackgroundColor(Color.WHITE)
        } else {
            holder.tvClipArt.setBackgroundColor(Color.BLACK)
        }

        holder.tvClipArt.setOnClickListener {
            //selected clip art will be saved here
            selectedPosition = position
            selectedFilePath = myClipArtList[holder.adapterPosition]
            notifyDataSetChanged()
        }
    }

    fun setClipArt() {
        if (selectedFilePath != null) {
            Log.v(tagName, "selectedFilePath: $selectedFilePath")
            myClipArtListener.selectedClipArt(selectedFilePath!!,type)
        }
    }

    class MyPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvClipArt: ImageView = itemView.findViewById(R.id.imgStiker)
    }
}