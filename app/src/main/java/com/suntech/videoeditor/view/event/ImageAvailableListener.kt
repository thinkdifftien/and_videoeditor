package com.suntech.recorder.view.event

import android.graphics.Bitmap
import android.media.Image
import android.media.ImageReader
import android.util.Log
import com.suntech.recorder.utils.ConstantUtils
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class ImageAvailableListener(private val mWidth: Int, private val mHeight: Int) : ImageReader.OnImageAvailableListener {
    companion object {
        val KEY_SCREEN_SHOT: String = "KEY_SCREEN_SHOT"
    }

    private var isSaved: Boolean = false
    lateinit var mCallback: OnActionCallBack

    override fun onImageAvailable(reader: ImageReader?) {
        if (isSaved) {
            return
        }
        Log.e("ScreenCapture", "onImageAvailable")
        var image: Image? = null
        var fos: FileOutputStream? = null
        var bitmap: Bitmap? = null

        try {
            image = reader!!.acquireLatestImage()
            if (image != null) {
                val planes = image.planes
                val buffer = planes[0].buffer
                val pixelStride = planes[0].pixelStride
                val rowStride = planes[0].rowStride
                val rowPadding: Int = rowStride - pixelStride * mWidth
                // create bitmap
                bitmap = Bitmap.createBitmap(
                    mWidth + rowPadding / pixelStride,
                    mHeight,
                    Bitmap.Config.ARGB_8888
                )
                bitmap.copyPixelsFromBuffer(buffer)
                val file = File(ConstantUtils.BASE_FILE + "/image/")
                if (!file.exists()){
                    file.mkdir()
                }
                // write bitmap to a file
                val filePath =
                    ConstantUtils.BASE_FILE + "/image/" + System.currentTimeMillis() + ".jpg"
                fos =
                    FileOutputStream(filePath)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                isSaved = true
                mCallback.callBack(KEY_SCREEN_SHOT, filePath)
            }

        } catch (e: Exception) {
            e.printStackTrace()
            mCallback.callBack(KEY_SCREEN_SHOT, null)
        } finally {
            if (fos != null) {
                try {
                    fos.close()
                } catch (ioe: IOException) {
                    ioe.printStackTrace()
                }
            }
            bitmap?.recycle()
            image?.close()
        }
    }
}