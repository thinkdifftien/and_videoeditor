package com.suntech.recorder.view.fragment

import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RadioGroup
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.otaliastudios.transcoder.Transcoder
import com.otaliastudios.transcoder.TranscoderListener
import com.otaliastudios.transcoder.engine.TrackStatus
import com.otaliastudios.transcoder.engine.TrackType
import com.otaliastudios.transcoder.sink.DataSink
import com.otaliastudios.transcoder.sink.DefaultDataSink
import com.otaliastudios.transcoder.source.DataSource
import com.otaliastudios.transcoder.source.UriDataSource
import com.otaliastudios.transcoder.strategy.TrackStrategy
import com.otaliastudios.transcoder.validator.DefaultValidator
import com.suntech.recorder.event.OnActionSTrancoder
import com.suntech.recorder.presenter.TrancoderPresenter

import com.suntech.recorder.view.base.BaseFragment
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.activity.EditActivity
import java.io.File
import java.io.IOException
import java.util.concurrent.Future

class TrancoderFrg : BaseFragment<TrancoderPresenter>() , OnActionSTrancoder,TranscoderListener{

    private lateinit var LLspeed : LinearLayout
    private lateinit var LLrotation : LinearLayout
    private lateinit var btnSave : Button
    private lateinit var btncancle : Button

    private var mTranscodeFuture: Future<Void>? = null
    private lateinit var  mTranscodeInputUri1: Uri
    private val mTranscodeInputUri2: Uri? = null
    private val mTranscodeInputUri3: Uri? = null
    private val mAudioReplacementUri: Uri? = null
    private lateinit var mTranscodeOutputFile: File
    private var mTranscodeStartTime: Long = 0
    private  var dialog : MaterialDialog ?=null

    private var mVideoRotationGroup: RadioGroup? = null
    private var mSpeedGroup: RadioGroup? = null
    private lateinit var btnCancle : Button

    private var speed : Float ? = null
    private  var rotation : Int?  = null

    private val mTranscodeVideoStrategy: TrackStrategy? = null
    private val mTranscodeAudioStrategy: TrackStrategy? = null

    private lateinit var unbinder: Unbinder
    private  var filepath : String ?=null

    companion object {
        private const val TYPE = "type"
        private const val FILE_PATH = "file_path"
        fun newInstance(position: Int,file:String): TrancoderFrg {
            val fragment = TrancoderFrg()
            val args = Bundle()
            args.putInt(TYPE, position)
            args.putString(FILE_PATH, file)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun getLayoutId(): Int {
        return  R.layout.fragment_trancoder
    }

    override fun initPresenter() {
        mPresenter = TrancoderPresenter(this)
    }

    override fun initView(v: View) {

        btnCancle = v.findViewById(R.id.btCancel)
        mVideoRotationGroup = v.findViewById(R.id.rotation)
        mSpeedGroup = v.findViewById(R.id.speed)
                        LLspeed = v.findViewById(R.id.LLspeed)
        LLrotation = v.findViewById(R.id.LLrotate)
        btnSave = v.findViewById(R.id.btSave)
        btnSave.setOnClickListener{
            showLoading()
            transcode()
        }
        btncancle = v.findViewById(R.id.btCancel)
        btncancle.setOnClickListener{
            var frg = MainMenuFrg()
            (activity as EditActivity) replacetoDefaultFragment(frg)
        }
        checkType()
    }

    private fun showLoading() {
        dialog = context?.let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }
    }

    private fun checkType() {
        var i = arguments?.get(TYPE)
        filepath = arguments!!.get(FILE_PATH)?.toString()
        if (i==1){
            LLrotation.visibility = GONE
        }
        else{
            LLspeed.visibility = GONE
        }
    }

    private fun transcode() {
        // Create a temporary file for output.
        try {
            val outputDir = File(activity?.getExternalFilesDir(null), "outputs")
            outputDir.mkdir()
            mTranscodeOutputFile = File.createTempFile("transcode_test", ".mp4", outputDir)
        } catch (e: IOException) {
            Toast.makeText(activity, "Failed to create temporary file.", Toast.LENGTH_LONG).show()
            return
        }

        when(mVideoRotationGroup?.checkedRadioButtonId){
            R.id.rotation_90 -> rotation = 90
            R.id.rotation_180 -> rotation = 180
            R.id.rotation_270 -> rotation = 270
            else -> rotation = 0
        }

        when(mSpeedGroup?.checkedRadioButtonId){
            R.id.speed_05x -> speed =0.5f
            R.id.speed_2x -> speed = 2f
            else -> speed = 1f
        }

        // Launch the transcoding operation.
        mTranscodeStartTime = SystemClock.uptimeMillis()

        val sink: DataSink = DefaultDataSink(mTranscodeOutputFile.getAbsolutePath())
        val builder = Transcoder.into(sink)

        mTranscodeInputUri1 = Uri.parse(filepath)
        if (mAudioReplacementUri == null) {
            if (mTranscodeInputUri1 != null) {
                val source: DataSource = UriDataSource(activity as EditActivity, mTranscodeInputUri1)
                builder.addDataSource(source)
            }
        } else {
            if (mTranscodeInputUri1 != null) {
                val source: DataSource = UriDataSource(activity as EditActivity,  mTranscodeInputUri1)
                builder.addDataSource(source)
            }
            builder.addDataSource(TrackType.AUDIO, activity as EditActivity, mAudioReplacementUri)
        }
        mTranscodeFuture = builder.setListener(this)
            .setAudioTrackStrategy(mTranscodeAudioStrategy)
            .setVideoTrackStrategy(mTranscodeVideoStrategy)
            .setVideoRotation(rotation!!)
            .setValidator(object : DefaultValidator() {
                override fun validate(videoStatus: TrackStatus, audioStatus: TrackStatus): Boolean {
                    return super.validate(videoStatus, audioStatus)
                }
            })
            .setSpeed(speed!!)
            .transcode()

    }

    override fun onTranscodeCompleted(successCode: Int) {
        dialog!!.dismiss()
        activity?.runOnUiThread(Runnable {
            (activity as EditActivity) updateVideo(mTranscodeOutputFile.absolutePath)
            (activity as EditActivity) replacetoDefaultFragment(MainMenuFrg())
        })

    }

    override fun onTranscodeProgress(progress: Double) {
    }

    override fun onTranscodeCanceled() {
        dialog!!.dismiss()
        onTranscodeFinished(false, "Transcoder canceled.")
    }

    override fun onTranscodeFailed(exception: Throwable) {
        dialog!!.dismiss()
        onTranscodeFinished(false, "Transcoder error occurred. " + exception.message)
    }

    private fun onTranscodeFinished(isSuccess: Boolean, toastMessage: String) {
        dialog!!.dismiss()
        Toast.makeText(activity, toastMessage, Toast.LENGTH_LONG).show()
    }
}