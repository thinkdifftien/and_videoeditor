package com.suntech.videoeditor.view.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.loader.content.CursorLoader
import com.suntech.recorder.view.base.BaseActivity
import com.suntech.videoeditor.App
import com.suntech.videoeditor.R
import com.suntech.videoeditor.utils.GifSizeFilter
import com.suntech.videoeditor.widget.Glide4Engine
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.filter.Filter

class MainActivity : BaseActivity<Any>(), View.OnClickListener {
    private var frAds: FrameLayout? = null
    private lateinit var btn_myvideo : LinearLayout
    private lateinit var btn_videoeditor : LinearLayout
    private lateinit var btn_slideshow : LinearLayout
    private lateinit var btn_setting : LinearLayout
    private lateinit var btn_camera : ImageView
    private val REQUEST_CODE_CHOOSE = 23

    override fun initPresenter() {
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun onStart() {
        super.onStart()
        Handler().postDelayed(Runnable {
            requestPermissions(
                arrayOf(
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.CAMERA
                ), 1
            )
        }, 2000)
    }


    override fun initView() {
        addEvent()
        frAds = findViewById<FrameLayout>(R.id.fr_ads)
        btn_camera = findViewById(R.id.btn_camera)
        btn_camera.setOnClickListener(this)
        btn_myvideo = findViewById(R.id.btn_myvideo)
        btn_myvideo.setOnClickListener(this)
        btn_videoeditor = findViewById(R.id.btn_videoeditor)
        btn_videoeditor.setOnClickListener(this)
        btn_slideshow = findViewById(R.id.btn_slideshow)
        btn_slideshow.setOnClickListener(this)
        btn_setting = findViewById(R.id.btnSetting)
        btn_setting.setOnClickListener(this)
//        App.admod.loadNativeAds(this, frAds!!)
//        val path = getExternalFilesDir("video_ne")
//
//        if (!path!!.exists())
//            path.mkdirs();
//        Log.e("path_ne",path.absolutePath)
    }



    private fun addEvent() {

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btn_myvideo -> {
                val intent = Intent(this,MyVideoActivity::class.java)
                startActivity(intent)
                App.admod.loadInterialAds()
            }
            R.id.btn_videoeditor -> {
                Matisse.from(this)
                    .choose(MimeType.ofVideo(), true)
                    .countable(true)
                    .maxSelectable(1)
                    .addFilter(GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
                    .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
                    .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                    .thumbnailScale(0.85f)
                    .showSingleMediaType(true)
                    .imageEngine(Glide4Engine())
                    .showPreview(false) // Default is `true`
                    .forResult(REQUEST_CODE_CHOOSE)
            }
            R.id.btn_slideshow -> {
                val intent = Intent(this,SlideActivity::class.java)
                startActivity(intent)
                App.admod.loadInterialAds()
            }
            R.id.btnSetting -> {
                val intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
                App.admod.loadInterialAds()
            }
            R.id.btn_camera -> {
                val intent = Intent(this,CameraActivity::class.java)
                startActivity(intent)
                App.admod.loadInterialAds()
            }
        }
    }


    override fun updateUI(tag: String?) {
        when (tag) {

        }
    }

    private fun updateUI(view: ImageView, image: Int) {
        resetUI()
        view.setImageResource(image)
    }

    private fun resetUI() {

    }

    override fun onBackPressed() {
        backInMain()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == Activity.RESULT_OK) {
            var path = Matisse.obtainResult(data)
            val intent = Intent(this, EditActivity::class.java)
            intent.putExtra("img_path",getRealPathFromURI(path.get(0)))
            intent.putExtra("edit",true)
            Log.v("path",getRealPathFromURI(path.get(0)))
            startActivity(intent)
            App.admod.loadInterialAds()
        }

    }

    private fun getRealPathFromURI(contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val loader = CursorLoader(this, contentUri, proj, null, null, null)
        val cursor: Cursor = loader.loadInBackground()!!
        val column_index: Int =
            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val result: String = cursor.getString(column_index)
        cursor.close()
        return result
    }

    override fun callBack(key: String, vararg obj: Any?) {

    }
}
