package com.suntech.recorder.view.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog
import com.daasuu.mp4compose.FillMode
import com.daasuu.mp4compose.Rotation
import com.daasuu.mp4compose.composer.Mp4Composer

import com.suntech.recorder.event.OnACtionEditor
import com.suntech.recorder.interfaces.AddFilterListener
import com.suntech.recorder.presenter.EditorPresenter
import com.suntech.recorder.utils.FilterSave

import com.suntech.recorder.view.adapter.AddFilterAdapter
import com.suntech.recorder.view.base.BaseFragment
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.activity.EditActivity
import kotlinx.android.synthetic.main.frg_filters.view.*
import java.io.File


class FilterFragment : BaseFragment<EditorPresenter>(), OnACtionEditor, AddFilterListener {

    private lateinit var unbinder: Unbinder
    private lateinit var mAppName: String
    private lateinit var mAppPath: File
    private  var filepath : String ?=null
    private lateinit var filename : String
    private lateinit var filterFilepath : String

    private var mPosition : Int = 0

    companion object {
        private const val TYPE_FILTER = "type_filter"
        fun newInstance(position: String): FilterFragment {
            val fragment = FilterFragment()
            val args = Bundle()
            args.putString(TYPE_FILTER, position)
            fragment.setArguments(args)
            return fragment
        }
    }

    @OnClick(R.id.btCancel)
    internal fun cancle() {
        var frg = MainMenuFrg()
        (activity as EditActivity) replacetoDefaultFragment(frg)
        (activity as EditActivity)updateVideo(0)
    }

    @OnClick(R.id.btSave)
    internal fun save() {
        saveVideoWithFilter()
    }

    override fun initPresenter() {
        mPresenter = EditorPresenter(this)
    }

    override fun initView(v: View) {

            filepath = arguments!!.get(TYPE_FILTER)?.toString()
            unbinder = ButterKnife.bind(this, v)

            mAppName = getString(R.string.app_name)
            mAppPath = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES), mAppName)
            Log.e("mAppPath",mAppPath.absolutePath)
            Log.e("path",filepath)
            filename = "video_edit.mp4"
            filterFilepath = "$mAppPath/$filename"
            Log.e("filterFilepath",filterFilepath)
            val adapter = AddFilterAdapter(this, "filepath")

            v.recyclerView.apply {
            isNestedScrollingEnabled = false
            layoutManager = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)
            this.adapter = adapter
            onFlingListener = null
        }
    }

    override fun getLayoutId(): Int {
      return R.layout.frg_filters
    }

    override fun onClick(v: View, position: Int) {
        (activity as EditActivity)updateVideo(position)
        mPosition = position
    }

     fun saveVideoWithFilter() {
        if (!mAppPath.exists()) {
            mAppPath.mkdirs()
        }
        val dialog = context?.let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }

        if (mPosition != 0) {
            Mp4Composer(filepath.toString(), filterFilepath)
                .rotation(Rotation.NORMAL)
                .fillMode(FillMode.PRESERVE_ASPECT_FIT)
                .filter(FilterSave.createGlFilter(FilterSave.createFilterList()[mPosition], context))
                .listener(object : Mp4Composer.Listener {
                    override fun onProgress(progress: Double) {
                        Log.d(mAppName, "onProgress Filter = " + progress*100)
                    }
                    override fun onCompleted() {
                        activity?.runOnUiThread(Runnable {
                            (activity as EditActivity) updateVideo(filterFilepath)
                            (activity as EditActivity) replacetoDefaultFragment(MainMenuFrg())
                        })

                        dialog?.dismiss()
                    }
                    override fun onCanceled() {
                        dialog?.dismiss()
                        Log.d(mAppName, "onCanceled")
                    }
                    override fun onFailed(exception: Exception) {
                        dialog?.dismiss()
                        Log.e(mAppName, "onFailed() Filter", exception)
                    }
                })
                .start()
        } else {
            dialog?.dismiss()
            Toast.makeText(context, "Not use filter", Toast.LENGTH_SHORT).show()
        }
    }

}