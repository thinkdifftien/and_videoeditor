package com.suntech.videoeditor.view.activity


import android.app.Activity
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.provider.MediaStore
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.afollestad.materialdialogs.MaterialDialog
import com.daasuu.epf.EPlayerView
import com.daasuu.mp4compose.FillMode
import com.daasuu.mp4compose.composer.Mp4Composer
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.FFmpegLoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.otaliastudios.transcoder.Transcoder
import com.otaliastudios.transcoder.TranscoderListener
import com.otaliastudios.transcoder.engine.TrackStatus
import com.otaliastudios.transcoder.engine.TrackType
import com.otaliastudios.transcoder.sink.DataSink
import com.otaliastudios.transcoder.sink.DefaultDataSink
import com.otaliastudios.transcoder.source.DataSource
import com.otaliastudios.transcoder.source.TrimDataSource
import com.otaliastudios.transcoder.source.UriDataSource
import com.otaliastudios.transcoder.strategy.TrackStrategy
import com.otaliastudios.transcoder.validator.DefaultValidator
import com.suntech.recorder.event.OnACtionEditor
import com.suntech.recorder.interfaces.FFMpegCallback
import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Video
import com.suntech.recorder.presenter.EditorPresenter
import com.suntech.recorder.utils.FilterType
import com.suntech.recorder.utils.OptiUtils
import com.suntech.recorder.utils.VideoToGIF
import com.suntech.recorder.view.base.BaseActivity
import com.suntech.recorder.view.dialog.GIFDialog
import com.suntech.recorder.view.fragment.*
import com.suntech.videoeditor.App
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.fragment.OptiAddBackgroundFragment
import kotlinx.android.synthetic.main.activity_edit.*
import net.vrgsoft.videcrop.VideoCropActivity.createIntent
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Future


class EditActivity : BaseActivity<EditorPresenter>(), OnACtionEditor, TranscoderListener, OptiBaseCreatorDialogFragment.CallBacks, FFMpegCallback {

    val REQUEST_CODE_PICK_AUDIO = 1
    private var mTranscodeFuture: Future<Void>? = null
    private lateinit var  mTranscodeInputUri1: Uri
    private val mTranscodeInputUri2: Uri? = null
    private val mTranscodeInputUri3: Uri? = null
    private lateinit var mAudioReplacementUri: Uri
    private lateinit var mTranscodeOutputFile: File
    private var mTranscodeStartTime: Long = 0
    private val mTranscodeVideoStrategy: TrackStrategy? = null
    private val mTranscodeAudioStrategy: TrackStrategy? = null
    private var mIsAudioOnly = false
    private lateinit var btnBack : ImageView
    private lateinit var btnDone : ImageView
    private lateinit var mAppName: String
    private lateinit var mAppPath: File
    lateinit var player: SimpleExoPlayer
    lateinit var ePlayerView: EPlayerView
    private lateinit var filepath : String
    private lateinit var fileImput : String
    private lateinit var filterFilepath : String
    private var mPosition : Int = 0
    private lateinit var media : MediaPlayer
    private var isCrop = false
    private var isEdit = false
    private  var dialog : MaterialDialog ?=null
    val CROP_REQUEST  : Int = 200
    val outputPath = "/storage/emulated/0/YDXJ08599.mp4"
    var fragment: Fragment? = null

    companion object {
        internal const val REQUEST_VIDEO_TRIMMER = 2
        private const val REQUEST_STORAGE_READ_ACCESS_PERMISSION = 2
        internal const val EXTRA_INPUT_URI = "EXTRA_INPUT_URI"
        private val allowedVideoFileExtensions = arrayOf("mkv", "mp4", "3gp", "mov", "mts")
        private val videosMimeTypes = ArrayList<String>(allowedVideoFileExtensions.size)
    }

    override fun initPresenter() {
        mPresnter = EditorPresenter(this)
    }

    override fun  initView() {

        btnBack = findViewById(R.id.btnBack)
        btnBack.setOnClickListener {
            finish()
        }
        btnDone = findViewById(R.id.btnDone)
        btnDone.setOnClickListener {
            saveVideo()
        }

        try {
            FFmpeg.getInstance(this).loadBinary(object : FFmpegLoadBinaryResponseHandler {
                override fun onFailure() {
                    Log.v("FFMpeg", "Failed to load FFMpeg library.")
                }

                override fun onSuccess() {
                    Log.v("FFMpeg", "FFMpeg Library loaded!")
                }

                override fun onStart() {
                    Log.v("FFMpeg", "FFMpeg Started")
                }

                override fun onFinish() {
                    Log.v("FFMpeg", "FFMpeg Stopped")
                }
            })
        } catch (e: FFmpegNotSupportedException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }


        mAppName = getString(R.string.app_name)
        filepath =intent.getStringExtra("img_path")
        isEdit = intent.getBooleanExtra("edit",false)
        if(isEdit && isEdit!=null){
            fileImput = getVideoFilePath()

        }
        else{
            fileImput = intent.getStringExtra("img_path")
        }

        fragment = MainMenuFrg()
        if(fragment!=null){
            replaceFragment(fragment!!)
        }
        itemStorageAction()
    }

    fun getVideoFilePath(): String {
        val parentFolder = getExternalFilesDir(null)!!

        parentFolder.mkdirs()
        val path = File(parentFolder.absolutePath,"/video")
        val fileName = SimpleDateFormat("yyyyMM_dd-HHmmss").format(Date()) + "cameraRecorder.mp4"
        val trimmedVideoFile = File(path.absolutePath, fileName)
        return trimmedVideoFile.absolutePath
    }

    private fun saveVideo() {
        showLoading()
        Mp4Composer(filepath, fileImput)
            .fillMode(FillMode.PRESERVE_ASPECT_FIT)
            .listener(object : Mp4Composer.Listener {
                override fun onProgress(progress: Double) {
                    Log.d(mAppName, "onProgress Filter = " + progress*100)
                }
                override fun onCompleted() {
                    val file = File(filepath)
                    val thumbnail = ThumbnailUtils.createVideoThumbnail(
                        file.path,
                        MediaStore.Video.Thumbnails.MICRO_KIND
                    )
                    val lastModDate = Date(file.lastModified())
                    val time = getTime(file.path)
                    val size = file.length()

                    var name: String
                    name = file.path.substring(
                        file.path.lastIndexOf("/") + 1,
                        file.path.length
                    ).replace(".mp4", "")
                    val media: Media = Video(
                        thumbnail,
                        time,
                        "${lastModDate.time}",
                        name,
                        file.path,
                        size,
                        lastModDate.time
                    )
                    App.storageCommon.addMedia(media)
                    dialog?.dismiss()
                    finish()
                }
                override fun onCanceled() {
                    dialog?.dismiss()
                    Log.e("haha", "onCanceled")
                }
                override fun onFailed(exception: Exception) {
                    dialog?.dismiss()
                    Log.e("haha", "onFailed() Filter", exception)
                }
            })
            .start()
    }

    private fun getTime(path: String): Long {
        var retriever = MediaMetadataRetriever();
        val timeInMillisec = try {
            retriever.setDataSource(App.instance, Uri.fromFile(File(path)));
            val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            retriever.release()
            time.toLong()
        } catch (e: Exception) {
            return 0
        }
        return timeInMillisec
    }

    private fun replaceFragment(frg: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layout_container, frg)
        transaction.commit()
    }

     infix fun replacetoDefaultFragment(frg: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.layout_container, frg)
        transaction.commit()
    }

    private fun startTrimActivity(string: String) {
        val intent = Intent(this, TrimmerActivity::class.java)
        intent.putExtra(EXTRA_INPUT_URI, string)
        startActivityForResult(intent,REQUEST_VIDEO_TRIMMER)
    }

    fun getMenuFragment(type : Int){
        when(type){
            0 -> {
                showLoading()
                var file = File(filepath)
                VideoToGIF.with(this)
                    .setFile(file)
                    .setOutputPath(OptiUtils.outputPath + "images")
                    .setOutputFileName("myGif_" + System.currentTimeMillis() + ".gif")
                    .setDuration("5") //Gif duration
                    .setScale("500") //Size of GIF
                    .setFPS("10") //Frame rate of GIF
                    .setCallback(this)
                    .create()

            }
            1 -> {
                startTrimActivity(filepath)
            }
            2 -> {
                fragment = FilterFragment.newInstance(filepath)
                replaceFragment(fragment!!)
            }
            3 -> {
                fragment = TrancoderFrg.newInstance(1,filepath)
                replaceFragment(fragment as TrancoderFrg)
            }
            4 -> {
                startActivityForResult(createIntent(this, filepath, outputPath),CROP_REQUEST)
            }
            5 -> {
                startActivityForResult(Intent(Intent.ACTION_GET_CONTENT).setType("audio/*"),REQUEST_CODE_PICK_AUDIO)
            }
            6 -> {
                fragment = TrancoderFrg.newInstance(2,filepath)
                replaceFragment(fragment as TrancoderFrg)
            }
            7 -> {
                val addClipArtFragment = OptiAddClipArtFragment()
                addClipArtFragment.setHelper(this)
                var file = File(filepath)
                addClipArtFragment.setFilePathFromSource(file)
                showBottomSheetDialogFragment(addClipArtFragment)
            }
            8 -> {
                val addBackgroundFragment = OptiAddBackgroundFragment()
                addBackgroundFragment.setHelper(this)
                var file = File(filepath)
                addBackgroundFragment.setFilePathFromSource(file)
                showBottomSheetDialogFragment(addBackgroundFragment)
            }
        }
    }

    private fun showBottomSheetDialogFragment(bottomSheetDialogFragment: BottomSheetDialogFragment) {
        val bundle = Bundle()
        bottomSheetDialogFragment.arguments = bundle
        val transaction = supportFragmentManager.beginTransaction()
        bottomSheetDialogFragment.show(transaction, bottomSheetDialogFragment.tag)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_VIDEO_TRIMMER && resultCode == RESULT_OK) {
            filepath = data!!.getStringExtra("PATH")
            updateVideo(filepath)
        }
        else if (requestCode == CROP_REQUEST && resultCode == RESULT_OK) {
                isCrop = true
        }
        else if(requestCode == REQUEST_CODE_PICK_AUDIO && resultCode == Activity.RESULT_OK && data != null && data.data != null){
            showLoading()
            mAudioReplacementUri = data.data!!
            transcode()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    private fun transcode() {
        try {
            val outputDir = File(getExternalFilesDir(null), "outputs")
            outputDir.mkdir()
            mTranscodeOutputFile = File.createTempFile("transcode_test", ".mp4", outputDir)
            Log.v("link",mTranscodeOutputFile.absolutePath)
        } catch (e: IOException) {
            Toast.makeText(this, "Failed to create temporary file.", Toast.LENGTH_LONG).show()
            return
        }

        var rotation = 0
        var speed = 1f


        // Launch the transcoding operation.
        mTranscodeStartTime = SystemClock.uptimeMillis()

        val sink: DataSink = DefaultDataSink(mTranscodeOutputFile.getAbsolutePath())
        val builder = Transcoder.into(sink)

        mTranscodeInputUri1 = Uri.parse(filepath)

            if (mTranscodeInputUri1 != null) {
                val source: DataSource = UriDataSource(this,  mTranscodeInputUri1)
                builder.addDataSource(TrackType.VIDEO, TrimDataSource(source, 0, 0))
            }
            builder.addDataSource(TrackType.AUDIO, this, mAudioReplacementUri)

        mTranscodeFuture = builder.setListener(this)
            .setAudioTrackStrategy(mTranscodeAudioStrategy)
            .setVideoTrackStrategy(mTranscodeVideoStrategy)
            .setVideoRotation(rotation!!)
            .setValidator(object : DefaultValidator() {
                override fun validate(videoStatus: TrackStatus, audioStatus: TrackStatus): Boolean {
                    return super.validate(videoStatus, audioStatus)
                    mIsAudioOnly = !videoStatus.isTranscoding
                    return super.validate(videoStatus, audioStatus)
                }
            })
            .setSpeed(speed!!)
            .transcode()

    }

    override fun getLayoutId(): Int {
        return R.layout.activity_edit
    }

    override fun onResume() {
        super.onResume()
        if(isCrop){
            updateVideo(outputPath)
        }
        else{
            setUpSimpleExoPlayer()
            setUpGlPlayerView()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        filterView.removeAllViews()
        ePlayerView.onPause()
        player.stop()
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    private fun releasePlayer() {
        filterView.removeAllViews()
        ePlayerView.onPause()
        player.stop()
        player.release()
    }

    private fun setUpSimpleExoPlayer() {

        val dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, mAppName))
        val videoSource = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(filepath))
        player = ExoPlayerFactory.newSimpleInstance(this).apply {
            prepare(videoSource)
            playWhenReady = true
            repeatMode = Player.REPEAT_MODE_ONE

        }

    }

    infix fun updateVideo(file : String){
        filterView.removeAllViews()
        filepath = file
        releasePlayer()
        val dataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, mAppName))
        val videoSource = ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(Uri.parse(filepath))
        player = ExoPlayerFactory.newSimpleInstance(this).apply {
            prepare(videoSource)
            playWhenReady = true
            repeatMode = Player.REPEAT_MODE_ONE
        }
        ePlayerView = EPlayerView(this).apply { setSimpleExoPlayer(player)}
        filterView.addView(ePlayerView)
        ePlayerView.onResume()

    }

    private fun setUpGlPlayerView() {
        ePlayerView = EPlayerView(this).apply { setSimpleExoPlayer(player)}
        filterView.addView(ePlayerView)
        ePlayerView.onResume()

    }

    infix fun updateVideo(position: Int){
        mPosition = position
        ePlayerView.setGlFilter(FilterType.createGlFilter(FilterType.createFilterList()[mPosition], this))
    }

    private fun showLoading() {
        dialog = let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }
    }

    private fun itemStorageAction() {

            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_1,
                "sticker_1",
               this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_2,
                "sticker_2",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_3,
                "sticker_3",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_4,
                "sticker_4",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_5,
                "sticker_5",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_6,
                "sticker_6",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_7,
                "sticker_7",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_8,
                "sticker_8",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_9,
                "sticker_9",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_10,
                "sticker_10",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_11,
                "sticker_11",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_12,
                "sticker_12",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_13,
                "sticker_13",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_14,
                "sticker_14",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_15,
                "sticker_15",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_16,
                "sticker_16",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_17,
                "sticker_17",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_18,
                "sticker_18",
                this
            )
            OptiUtils.copyFileToInternalStorage(
                R.drawable.sticker_19,
                "sticker_19",
                this
            )

            OptiUtils.copyFontToInternalStorage(
                R.font.roboto_black,
                "roboto_black",
                this
            )
    }

    override fun onProgress(progress: String) {
        Log.v("tagName", "onProgress")
    }

    override fun onSuccess(convertedFile: File, type: String) {
        Log.v("tagName", convertedFile.absolutePath)
        GIFDialog.show(supportFragmentManager, convertedFile)
        dialog!!.dismiss()
    }

    override fun onFailure(error: Exception) {
        Log.v("tagName", "onFailure")
    }

    override fun onNotAvailable(error: Exception) {
        Log.v("tagName", "onNotAvailable")
    }

    override fun onFinish() {
        Log.v("tagName", "onFinish")
    }

    override fun onTranscodeCompleted(successCode: Int) {
        dialog!!.dismiss()


        updateVideo(mTranscodeOutputFile.absolutePath)
    }

    override fun onTranscodeProgress(progress: Double) {

    }

    override fun onTranscodeCanceled() {
        dialog!!.dismiss()
    }

    override fun onTranscodeFailed(exception: Throwable) {
        dialog!!.dismiss()
    }

    override fun onDidNothing() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFileProcessed(file: File) {
         updateVideo (file.absolutePath)
    }

    override fun getFile(): File? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun reInitPlayer() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onAudioFileProcessed(convertedAudioFile: File) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showLoading(isShow: Boolean) {

    }

    override fun openGallery() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openCamera() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

