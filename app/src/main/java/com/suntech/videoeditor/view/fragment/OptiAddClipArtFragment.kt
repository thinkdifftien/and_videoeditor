
package com.suntech.recorder.view.fragment

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.suntech.recorder.OptiVideoEditor
import com.suntech.recorder.interfaces.OptiClipArtListener
import com.suntech.recorder.interfaces.OptiFFMpegCallback
import com.suntech.recorder.interfaces.OptiPositionListener
import com.suntech.recorder.model.Menu
import com.suntech.recorder.utils.ConstantUtils
import com.suntech.recorder.utils.ConstantUtils.Companion.FOLDER_STICKER
import com.suntech.recorder.utils.OptiUtils
import com.suntech.recorder.view.adapter.MenuAdapter
import com.suntech.recorder.view.adapter.OptiClipArtAdapter
import com.suntech.recorder.view.adapter.OptiPositionAdapter
import com.suntech.videoeditor.R
import com.suntech.videoeditor.api.API
import com.suntech.videoeditor.interfaces.OptiStickerListerner
import com.suntech.videoeditor.interfaces.ResultUnzip
import com.suntech.videoeditor.model.Detail
import com.suntech.videoeditor.model.Detailmage
import com.suntech.videoeditor.model.Store
import com.suntech.videoeditor.view.adapter.PreviewAdapter
import com.suntech.videoeditor.view.adapter.StickerAdapter
import com.suntech.videoeditor.widget.DownloadZipfile
import kotlinx.android.synthetic.main.opti_fragment_add_clip_art.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class OptiAddClipArtFragment : BottomSheetDialogFragment(), OptiClipArtListener, OptiPositionListener, OptiFFMpegCallback,
    Callback<Store>, OptiStickerListerner {

    private var tagName: String = OptiAddClipArtFragment::class.java.simpleName

    private lateinit var linearLayoutManagerOne: LinearLayoutManager
    private lateinit var linearLayoutManagerTwo: LinearLayoutManager
    private lateinit var linearLayoutManagerThree: LinearLayoutManager
    private lateinit var rvClipArt: RecyclerView
    private lateinit var rvPosition: RecyclerView
    private lateinit var rvSticker: RecyclerView
    private lateinit var ivClose: ImageView
    private lateinit var ivDone: ImageView
    private var videoFile: File? = null
    private var clipArtFilePath: ArrayList<String> = ArrayList()
    private var positionList: ArrayList<String> = ArrayList()
    private var helper: OptiBaseCreatorDialogFragment.CallBacks? = null
    private lateinit var optiClipArtAdapter: OptiClipArtAdapter
    private lateinit var optiPositionAdapter: OptiPositionAdapter
    private lateinit var optiStickerAdapter: StickerAdapter
    private lateinit var previewAdapter: PreviewAdapter
    private var selectedPositionItem: String? = null
    private var selectedFilePath: String? = null
    private var assetFile: String? = null
    private var typeSticker: String? = null
    private var mContext: Context? = null
    private lateinit var rootView: View
    private var outputFile : File? =null
    private  var dialog : MaterialDialog ?=null
    private  var dialog2 : MaterialDialog ?=null
    private lateinit var arrSticker:  MutableList<Detail>
    private  var listDetailSore: MutableList<Detailmage> = ArrayList<Detailmage>()
    private var currentPosition = 0
    var downloadZipfile: DownloadZipfile? = null

    val stickerPath = arrayOf(
        "stickers/type1",
        "stickers/type2",
        "stickers/type3",
        "stickers/type4",
        "stickers/type5"
    )
    private  var pathList:  ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.opti_fragment_add_clip_art, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvClipArt = rootView.findViewById(R.id.rvClipArt)
        rvPosition = rootView.findViewById(R.id.rvPosition)
        rvSticker = rootView.findViewById(R.id.rc_sticker)
        ivClose = rootView.findViewById(R.id.iv_close)
        ivDone = rootView.findViewById(R.id.iv_done)
        linearLayoutManagerOne = LinearLayoutManager(activity!!.applicationContext)
        linearLayoutManagerTwo = LinearLayoutManager(activity!!.applicationContext)
        linearLayoutManagerThree = LinearLayoutManager(activity!!.applicationContext)

        linearLayoutManagerOne.orientation = LinearLayoutManager.HORIZONTAL
        rvClipArt.layoutManager = linearLayoutManagerOne
        linearLayoutManagerTwo.orientation = LinearLayoutManager.HORIZONTAL
        rvPosition.layoutManager = linearLayoutManagerTwo
        linearLayoutManagerThree.orientation = LinearLayoutManager.HORIZONTAL
        rc_sticker.layoutManager = linearLayoutManagerThree

        mContext = context

        val listFile: Array<File>

        val file = File(
            Environment.getExternalStorageDirectory(),
            File.separator + ConstantUtils.APP_NAME + File.separator + ConstantUtils.CLIP_ARTS + File.separator
        )

        if (file.isDirectory) {
            listFile = file.listFiles()
            for (i in listFile.indices) {
                clipArtFilePath.add(listFile[i].absolutePath)
            }
        }

        optiClipArtAdapter = OptiClipArtAdapter(clipArtFilePath, activity!!.applicationContext, ConstantUtils.TYPE_ASSETS,this)
        rvClipArt.adapter = optiClipArtAdapter
        optiClipArtAdapter.notifyDataSetChanged()

        positionList.add(ConstantUtils.BOTTOM_LEFT)
        positionList.add(ConstantUtils.BOTTOM_RIGHT)
        positionList.add(ConstantUtils.CENTRE)
        positionList.add(ConstantUtils.TOP_LEFT)
        positionList.add(ConstantUtils.TOP_RIGHT)

        optiPositionAdapter = OptiPositionAdapter(positionList, activity!!.applicationContext, this)
        rvPosition.adapter = optiPositionAdapter
        optiPositionAdapter.notifyDataSetChanged()

        ivClose.setOnClickListener {
            dismiss()
        }

        ivDone.setOnClickListener {

            optiClipArtAdapter.setClipArt()
            optiPositionAdapter.setPosition()

            if (selectedFilePath != null) {
                Log.v("file_path", selectedFilePath)
                if (selectedPositionItem != null) {
                    showLoading()
                    dismiss()
                    //apply clip art based on selected position
                    when (selectedPositionItem) {
                        ConstantUtils.BOTTOM_LEFT -> {
                            addClipArtAction(selectedFilePath!!, OptiVideoEditor.BOTTOM_LEFT)
                        }

                        ConstantUtils.BOTTOM_RIGHT -> {
                            addClipArtAction(selectedFilePath!!, OptiVideoEditor.BOTTOM_RIGHT)
                        }

                        ConstantUtils.CENTRE -> {
                            addClipArtAction(selectedFilePath!!, OptiVideoEditor.CENTER_ALLIGN)
                        }

                        ConstantUtils.TOP_LEFT -> {
                            addClipArtAction(selectedFilePath!!, OptiVideoEditor.TOP_LEFT)
                        }

                        ConstantUtils.TOP_RIGHT -> {
                            addClipArtAction(selectedFilePath!!, OptiVideoEditor.TOP_RIGHT)
                        }
                    }
                } else {
                    OptiUtils.showGlideToast(activity!!, getString(R.string.error_select_sticker_pos))
                }
            } else {
                OptiUtils.showGlideToast(activity!!, getString(R.string.error_select_sticker))
            }
        }
        loadDefaulSticker()
        API.apiService.sticker.enqueue(this)

    }

    private fun loadDefaulSticker() {
        clipArtFilePath = addStickerImages(stickerPath[0])
        optiClipArtAdapter = OptiClipArtAdapter(clipArtFilePath, activity!!.applicationContext,ConstantUtils.TYPE_ASSETS, this)
        rvClipArt.adapter = optiClipArtAdapter
        optiClipArtAdapter.notifyDataSetChanged()

    }

    @Throws(IOException::class)
    fun getFileFromAssets(fileName: String): File =
        File(mContext!!.cacheDir, fileName)
            .also {
                it.outputStream().use { cache ->
                    mContext!!.assets.open(fileName).use {
                        it.copyTo(cache)
                    }
                }
            }

    private fun addClipArtAction(imgPath: String, position: String) {
        //output file is generated and it is send to video processing
        outputFile = OptiUtils.createVideoFile(context!!)
        Log.v(tagName, "outputFile: ${outputFile!!.absolutePath}")

        OptiVideoEditor.with(context!!)
            .setType(ConstantUtils.VIDEO_CLIP_ART_OVERLAY)
            .setFile(videoFile!!)
            .setOutputPath(outputFile!!.path)
            .setImagePath(imgPath)
            .setPosition(position)
            .setCallback(this)
            .main()
    }

    fun setHelper(helper: OptiBaseCreatorDialogFragment.CallBacks) {
        this.helper = helper
    }

    fun setFilePathFromSource(file: File) {
        videoFile = file
        Log.v(tagName, file.absolutePath.toString())
    }

    override fun selectedPosition(position: String) {
        selectedPositionItem = position
    }

    override fun selectedClipArt(path: String,type: String) {
        val assetManager = context!!.assets
        selectedFilePath = path
        typeSticker = type
        if (type.equals(ConstantUtils.TYPE_ASSETS)){
            try {
                var inputstream: InputStream = assetManager.open(selectedFilePath!!)
                selectedFilePath = context!!.getExternalFilesDir(null).toString() + "/" +"clipArt.png"
                var outputstream = FileOutputStream(selectedFilePath)
                val buffer = ByteArray(1024)
                var read: Int = 0
                while (read != -1) {
                    outputstream.write(buffer, 0, read)
                    read = inputstream.read(buffer)
                }
                inputstream.close()
                outputstream.flush()
                outputstream.close()
            }catch (e: IOException){
                Toast.makeText(activity, e.printStackTrace().toString(),Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onProgress(progress: String) {
        Log.v(tagName, "onProgress()")
    }

    override fun onSuccess(convertedFile: File, type: String) {
        Log.v(tagName, "onSuccess()")
        dialog!!.dismiss()
        helper?.onFileProcessed(convertedFile)
    }


    override fun onFailure(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onFailure() ${error.localizedMessage}")
        Toast.makeText(mContext, "Video processing failed", Toast.LENGTH_LONG).show()

    }

    override fun onNotAvailable(error: Exception) {
        dialog!!.dismiss()
        Log.v(tagName, "onNotAvailable() ${error.localizedMessage}")
    }

    override fun onFinish() {
        dialog!!.dismiss()
    }

    private fun showLoading() {
        dialog = context?.let {
            MaterialDialog.Builder(it)
                .content(R.string.msg_dialog)
                .progress(true, 0)
                .cancelable(false)
                .show()
        }
    }

    override fun onFailure(call: Call<Store>, t: Throwable) {

    }

    override fun onResponse(call: Call<Store>, response: Response<Store>) {
        if(response==null|| response.body()==null){
            return
        }
        try {
            var data = response.body()!!.getData()
            var sticker = data!!.getSticker()
            arrSticker = sticker!!.getList() as MutableList<Detail>
            for (i in arrSticker.indices) {
                if (arrSticker.get(i).getMaterialType() == 0 && arrSticker.get(i).getDetailmages()!!.size > 3) {
                    var a = sticker.getList()!![i]
                    arrSticker.add(a!!)
                }
            }
            optiStickerAdapter = StickerAdapter(arrSticker, activity!!.applicationContext, this)
            rc_sticker.adapter = optiStickerAdapter
            optiPositionAdapter.notifyDataSetChanged()
        }catch (e: KotlinNullPointerException){

        }
    }

    override fun selectedSticker(name: String, binaryData: String, position: Int, isDownload: Boolean) {
        currentPosition = position
        clipArtFilePath.clear()
        if (arrSticker[position].isIsdowload()) {

            val file = File(Environment.getExternalStorageDirectory(), ConstantUtils.FOLDER_STICKER + name + "/")
            getSticker(file.absolutePath)
            Log.d("vaoday", file.absolutePath)

        }
        else if (position < 5) {
                clipArtFilePath = addStickerImages(stickerPath.get(position))
                optiClipArtAdapter = OptiClipArtAdapter(clipArtFilePath, activity!!.applicationContext,ConstantUtils.TYPE_ASSETS, this)
                rvClipArt.adapter = optiClipArtAdapter
                optiClipArtAdapter.notifyDataSetChanged()
        }
         else {
            showDialogPreview(name, binaryData)
        }
    }

    private fun showDialogPreview(name: String, binaryData: String) {
        if(listDetailSore!!.size!=null){
            listDetailSore!!.clear()
        }
        val dialog2 = Dialog(activity!!, android.R.style.Theme_Material_Light_Dialog)
        dialog2.setContentView(R.layout.dialog_preview)
        dialog2!!.setTitle(name)
        val rc_preview: RecyclerView = dialog2!!.findViewById(R.id.recyclerPreview)
        val tvComplete: TextView = dialog2!!.findViewById(R.id.tvComplete)
        val tvCancel: TextView = dialog2!!.findViewById(R.id.tvCancel)
        rc_preview.setHasFixedSize(true)
        rc_preview.layoutManager = GridLayoutManager(activity,3)
        for (i in arrSticker.indices) {
            var displayName = arrSticker[i].getDisplayName()
            if (name.equals(displayName, ignoreCase = true)) {
                listDetailSore.addAll(arrSticker[i].getDetailmages())
            }
        }
        previewAdapter = PreviewAdapter(listDetailSore, activity!!.applicationContext)
        rc_preview.adapter = previewAdapter
        previewAdapter.notifyDataSetChanged()
        tvComplete.setOnClickListener {
            downloadSticker(name, binaryData)
            dialog2!!.dismiss()
        }
        tvCancel.setOnClickListener {
            dialog2!!.dismiss()
        }
        dialog2!!.show()
    }

    private fun downloadSticker(name: String, binaryData: String) {
        val file = File(Environment.getExternalStorageDirectory(),  FOLDER_STICKER.toString() + name + "/")
        downloadZipfile = DownloadZipfile(activity, name, arrSticker, file.absolutePath+"/", object : ResultUnzip {
                override fun dataUnzip(file: String?) {
                    arrSticker[currentPosition].setIsdowload(true)
                    optiClipArtAdapter.notifyItemChanged(currentPosition)
                    getSticker(file!!)
                    Log.d("path",file)
                }
            })
        downloadZipfile!!.execute(binaryData)

    }
    private fun addStickerImages(folderPath: String): ArrayList<String> {
            try {
            val files = activity!!.assets.list(folderPath)
            for (name in files!!) {
                pathList.add(folderPath + File.separator + name)
                Log.d("path",folderPath + File.separator + name)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return pathList
    }


    private fun getSticker(file: String) {
        clipArtFilePath.clear()
        val directory = File(file)
        val files = directory.listFiles()
        for (i in files.indices) {
            if (files[i].name.contains("_MACOSX") || files[i].name.contains("config.json")) {
            } else {
                val file_image = file +"/"+ files[i].name
                Log.d("Files", file_image+ "")
                clipArtFilePath.add(file_image)
            }
        }

        optiClipArtAdapter = OptiClipArtAdapter(clipArtFilePath, activity!!.applicationContext,ConstantUtils.TYPE_DOWLOAD, this)
        rvClipArt.adapter = optiClipArtAdapter
        optiClipArtAdapter.notifyDataSetChanged()
    }
}