package com.suntech.recorder.view.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

import com.suntech.recorder.view.event.OnActionCallBack
import com.zaaach.toprightmenu.MenuItem
import com.zaaach.toprightmenu.TopRightMenu
import java.util.ArrayList

abstract class BaseFragment<T> : Fragment(), TopRightMenu.OnMenuItemClickListener {
    protected var mPresenter: T? = null
    lateinit var mCallBack: OnActionCallBack
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initPresenter()
        var v: View = inflater.inflate(getLayoutId(), container, false)
        initView(v)
        return v
    }

    abstract fun initPresenter()

    abstract fun initView(v: View)

    abstract fun getLayoutId(): Int

    override fun onMenuItemClick(position: Int) {

    }

}