package com.suntech.recorder.view.fragment

import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.loader.content.CursorLoader
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.afollestad.materialdialogs.MaterialDialog

import com.suntech.recorder.event.OnACtionEditor
import com.suntech.recorder.interfaces.FFMpegCallback
import com.suntech.recorder.presenter.EditorPresenter

import com.suntech.recorder.utils.OptiUtils

import com.suntech.recorder.view.base.BaseFragment
import com.suntech.recorder.view.dialog.GIFDialog
import com.suntech.videoeditor.R
import com.suntech.videoeditor.utils.GifSizeFilter
import com.suntech.videoeditor.view.activity.EditActivity
import com.suntech.videoeditor.view.activity.MainActivity
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.GlideEngine
import com.zhihu.matisse.filter.Filter
import java.io.File


class EditorFrg : BaseFragment<EditorPresenter>(), OnACtionEditor, FFMpegCallback {

    private lateinit var unbinder: Unbinder

    private val REQUEST_CODE_CHOOSE = 23
    private val REQUEST_CODE_CHOOSE_GIFT = 22
    val filePath: String? = null
    private  var dialog : MaterialDialog ?=null

//    @OnClick(R.id.btn_edit)
//    internal fun Edit() {
//        Matisse.from(this)
//            .choose(MimeType.ofVideo(), true)
//            .countable(true)
//            .maxSelectable(1)
//            .addFilter(GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//            .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
//            .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
//            .thumbnailScale(0.85f)
//            .showSingleMediaType(true)
//            .imageEngine(GlideEngine())
//            .showPreview(false) // Default is `true`
//            .forResult(REQUEST_CODE_CHOOSE)
//    }
//    @OnClick(R.id.btn_toGIF)
//    internal fun GIFT() {
//        Matisse.from(this)
//            .choose(MimeType.ofVideo(), true)
//            .countable(true)
//            .maxSelectable(1)
//            .addFilter(GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
//            .gridExpectedSize(resources.getDimensionPixelSize(R.dimen.grid_expected_size))
//            .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
//            .thumbnailScale(0.85f)
//            .showSingleMediaType(true)
//            .imageEngine(GlideEngine())
//            .showPreview(false) // Default is `true`
//            .forResult(REQUEST_CODE_CHOOSE_GIFT)
//    }


    override fun getLayoutId(): Int {
        return R.layout.frg_menu
    }

    companion object {
        val TAG: String = EditorFrg::class.java.name
    }

    override fun initPresenter() {
        mPresenter = EditorPresenter(this)
    }

    override fun initView(v: View) {
        unbinder = ButterKnife.bind(this, v)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder.unbind();
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_CHOOSE && resultCode == Activity.RESULT_OK) {
            var path = Matisse.obtainResult(data)

            val intent = Intent(activity, EditActivity::class.java)
            intent.putExtra("img_path",getRealPathFromURI(path.get(0)))
            Log.v("path",getRealPathFromURI(path.get(0)))
            startActivity(intent)
        }
        if (requestCode == REQUEST_CODE_CHOOSE_GIFT && resultCode == Activity.RESULT_OK) {
            showLoading()
            var path = Matisse.obtainResult(data)
            activity?.let {
                com.suntech.recorder.utils.VideoToGIF.with(it)
                    .setFile(File(getRealPathFromURI(path.get(0))))
                    .setOutputPath(OptiUtils.outputPath + "images")
                    .setOutputFileName("myGif_" + System.currentTimeMillis() + ".gif")
                    .setDuration("5") //Gif duration
                    .setScale("500") //Size of GIF
                    .setFPS("10") //Frame rate of GIF
                    .setCallback(this)
                    .create()
            }
        }

    }
    private fun getRealPathFromURI(contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val loader = CursorLoader(activity as MainActivity, contentUri, proj, null, null, null)
        val cursor: Cursor = loader.loadInBackground()!!
        val column_index: Int =
            cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val result: String = cursor.getString(column_index)
        cursor.close()
        return result
    }

    override fun onProgress(progress: String) {

    }

    override fun onSuccess(convertedFile: File, type: String) {
        GIFDialog.show(activity!!.supportFragmentManager, convertedFile)
        Log.v("tagName", "onSuccess")
        dialog!!.dismiss()
    }

    override fun onFailure(error: Exception) {
        Log.v("tagName", error.message)
        dialog!!.dismiss()
    }

    override fun onNotAvailable(error: Exception) {

    }

    override fun onFinish() {
        dialog!!.dismiss()
    }

    private fun showLoading() {
        dialog = let {
            activity?.let { it1 ->
                MaterialDialog.Builder(it1)
                    .content(R.string.gif_dialog)
                    .progress(true, 0)
                    .cancelable(false)
                    .show()
            }
        }
    }
}