package com.suntech.recorder.view.dialog

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.suntech.recorder.utils.OptiUtils
import com.suntech.videoeditor.R
import pl.droidsonroids.gif.GifDrawable
import pl.droidsonroids.gif.GifImageView
import java.io.File


/**
 * Created by umair on 18/01/2018.
 */
class GIFDialog : DialogFragment() {

    companion object {
        val TAG = GIFDialog::javaClass.name

        lateinit var file: File

        fun show(fragmentManager: FragmentManager, file: File) {
            this.file = file
            GIFDialog().show(fragmentManager, TAG)
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val view = activity!!.layoutInflater.inflate(R.layout.dialog_gif_preview, null)

        val gifView = view.findViewById<GifImageView>(R.id.gif_view)


        val gif = GifDrawable(file.path)
        gifView.setImageDrawable(gif)
        gif.start()

        return AlertDialog.Builder(activity)
                .setView(view)
                .setTitle("Preview")
                .setPositiveButton("Share") { dialog, which ->
                    Log.v("tagName",file.path)
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "image/gif"
                    val uri = Uri.fromFile(file.absoluteFile)
                    shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
                    startActivity(Intent.createChooser(shareIntent, "Share Emoji"))
                }
                .create()
    }
}
