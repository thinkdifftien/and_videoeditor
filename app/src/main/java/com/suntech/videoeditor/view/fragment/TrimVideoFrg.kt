package com.suntech.recorder.view.fragment


import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.suntech.videoeditor.R
import com.suntech.videoeditor.view.activity.EditActivity
import com.video_trim.K4LVideoTrimmer
import com.video_trim.interfaces.OnK4LVideoListener
import com.video_trim.interfaces.OnTrimVideoListener

class TrimVideoFrg : Fragment() ,OnTrimVideoListener,OnK4LVideoListener {

    private lateinit var mK4LVideoTrimmer : K4LVideoTrimmer
    private lateinit var mProgressView: ProgressDialog
    private  var filepath : String ?=null

    companion object {
        private const val TYPE_TRIM = "type_trim"
        fun newInstance(position: String): TrimVideoFrg {
            var fragment = TrimVideoFrg()
            var args = Bundle()
            args.putString(TYPE_TRIM, position)
            fragment.setArguments(args)
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v: View = inflater.inflate(R.layout.fragment_trimer, container, false)
        innit(v)
        return v
    }


    fun innit(v: View) {
        //setting progressbar

            filepath = arguments!!.get(TYPE_TRIM)?.toString()

            mProgressView = ProgressDialog(activity)
            mProgressView.setCancelable(false)
            mProgressView.setMessage(getString(R.string.trimming_progress))

        if (mK4LVideoTrimmer != null) {
            mK4LVideoTrimmer.setMaxDuration(1000)
            mK4LVideoTrimmer.setOnTrimVideoListener(this)
            mK4LVideoTrimmer.setOnK4LVideoListener(this)
            //mVideoTrimmer.setDestinationPath("/storage/emulated/0/DCIM/CameraCustom/");
            mK4LVideoTrimmer.setVideoURI(Uri.parse(filepath))
            mK4LVideoTrimmer.setVideoInformationVisibility(false)
        }
    }

    override fun onTrimStarted() {
        mProgressView.show()
    }

    @SuppressLint("StringFormatInvalid")
    override fun getResult(uri: Uri?) {
        mProgressView.cancel()
        activity?.runOnUiThread(Runnable {
            Toast.makeText(activity,uri!!.path, Toast.LENGTH_SHORT).show()
            (activity as EditActivity) replacetoDefaultFragment(MainMenuFrg())
            if (uri != null) {
                (activity as EditActivity) updateVideo(uri.path!!)
            }
        })

    }

    override fun onError(message: String?) {
        activity?.runOnUiThread(Runnable {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        })
    }

    override fun cancelAction() {
        (activity as EditActivity) replacetoDefaultFragment(MainMenuFrg())
    }

    override fun onVideoPrepared() {
        activity?.runOnUiThread(Runnable {

        })
    }


}