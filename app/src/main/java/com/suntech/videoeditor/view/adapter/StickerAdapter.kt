package com.suntech.videoeditor.view.adapter

import android.content.Context
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.suntech.recorder.utils.ConstantUtils.Companion.FOLDER_STICKER
import com.suntech.recorder.view.adapter.OptiClipArtAdapter
import com.suntech.videoeditor.R
import com.suntech.videoeditor.interfaces.OptiStickerListerner
import com.suntech.videoeditor.model.Detail
import com.suntech.videoeditor.model.SquareMainCover
import com.suntech.videoeditor.widget.FileManager.countFile
import java.io.File

class StickerAdapter(stickerList: MutableList<Detail>, val context: Context, optiStickerListener: OptiStickerListerner) :
    RecyclerView.Adapter<StickerAdapter.MyPostViewHolder>() {

    private var optiStickerListener = optiStickerListener
    private var tagName: String = OptiClipArtAdapter::class.java.simpleName
    private var myStickerList = stickerList
    private val mContext: Context? = null
    private var mainCover: SquareMainCover? = null
    private val isDownload = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyPostViewHolder {
        return MyPostViewHolder(LayoutInflater.from(context).inflate(R.layout.item_sticker, parent, false))
    }

    override fun getItemCount(): Int {
        return myStickerList.size
    }

    override fun onBindViewHolder(holder: MyPostViewHolder, position: Int) {
        var item = myStickerList.get(position)
        val file = File(Environment.getExternalStorageDirectory(), FOLDER_STICKER + item.getDisplayName().toString() + "/")
        if (countFile(file.absolutePath) > 0) {
            item.setIsdowload(true)
        }
        if (item.isIsdowload() == true || position < 5) {
            holder.download.visibility = View.GONE
        } else {
            holder.download.visibility = View.VISIBLE
        }
        mainCover = item.getSquareMainCover()
        val Image: ImageView = holder.tvSticker
        val url = mainCover!!.getUrl()
        Picasso.get()
            .load(url)
            .resize(150, 150)
            .centerCrop()
            .into(Image)

        holder.sticker.setOnClickListener {
            optiStickerListener.selectedSticker(item.getDisplayName()!!, item.getBinaryData()!!,holder.adapterPosition,isDownload)
        }
    }

    class MyPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvSticker: ImageView = itemView.findViewById(R.id.imgSticker)
        var download: RelativeLayout = itemView.findViewById(R.id.view_download)
        var sticker: RelativeLayout = itemView.findViewById(R.id.sticker)
    }
}