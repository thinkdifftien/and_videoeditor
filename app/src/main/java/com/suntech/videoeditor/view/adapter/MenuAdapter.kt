package com.suntech.recorder.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Menu
import com.suntech.recorder.model.Photo
import com.suntech.recorder.view.base.BaseAdapter
import com.suntech.recorder.view.base.BaseViewHolder
import com.suntech.videoeditor.R

class MenuAdapter(mList: MutableList<Menu>, mContext: Context) : BaseAdapter() {

    companion object {
        val KEY_MENU: String = "KEY_MENU"
    }
    private var mList = mList
    private var mContext = mContext


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var view = LayoutInflater.from(mContext).inflate(R.layout.item_menu, parent, false)
        return MenuViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var mHolder = holder as MenuViewHolder
        var menu = mList.get(position) as Menu

        mHolder.tv_menu.text = menu.name
        mHolder.ivPhoto.setImageBitmap(menu.thumbnail)


        mHolder.itemView.setOnClickListener(View.OnClickListener {
            mCallBack.callBack(KEY_MENU, position)
        })

    }
    class MenuViewHolder(itemView: View) : BaseViewHolder(itemView) {
        var ivPhoto = itemView.findViewById<ImageView>(R.id.img_menu)
        var tv_menu = itemView.findViewById<TextView>(R.id.tv_menu)
    }
}