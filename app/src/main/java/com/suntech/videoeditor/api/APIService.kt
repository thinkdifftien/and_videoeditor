package com.suntech.videoeditor.api

import com.suntech.videoeditor.model.Store
import com.suntech.videoeditor.model.Thumbail
import retrofit2.Call
import retrofit2.http.GET

interface APIService {
    @get:GET("getPreAll?page=1&app=pcp&pageSize=1000&version=10407")
    val sticker: Call<Store>

    @get:GET("tag.php?loc=vi&n=20&s=0&t=Colorful")
    val thumbail: Call<List<Thumbail>>
}