package com.suntech.recorder.presenter

import android.os.Handler
import android.os.Message
import com.suntech.recorder.event.OnCallBack

abstract class BasePresenter<T : OnCallBack> {
    protected var mCallBack: T

    private var mHandler: Handler = Handler(Handler.Callback { msg: Message ->
        excuteHandler(msg)
        return@Callback false
    })

    open protected fun excuteHandler(msg: Message) {

    }

    protected fun updateHandler(what: Int, arg1: Int = 0, arg2: Int = 0) {
        val msg = Message()
        msg.target = mHandler
        msg.what = what
        msg.arg1 = arg1
        msg.arg2 = arg2
        msg.sendToTarget()
    }


    constructor(mCallBack: T) {
        this.mCallBack = mCallBack
    }


}