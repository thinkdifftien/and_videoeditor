package com.suntech.recorder.presenter

import android.os.Message
import com.suntech.recorder.event.OnActionPhoto
import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Photo
import com.suntech.recorder.utils.ConstantUtils
import java.io.File
import java.util.*


class PhotoPresenter(mCallBack: OnActionPhoto) : BasePresenter<OnActionPhoto>(mCallBack) {
    var imageList: MutableList<Media> = ArrayList()

    private val REQUEST_UPDAT_PHOTO_LIST: Int = 1002

    fun getScreenShotList() {
        Thread(Runnable {
            excuteLoadPhoto()
        }).start()
    }

    private fun excuteLoadPhoto() {
        imageList.clear()
        val file = File(ConstantUtils.BASE_FILE + "/image")
        if (!file.exists()) {
            file.mkdir()
        }
        val listFile = file.listFiles()
        listFile ?: return
        listFile.reverse()

        for (i in 0..listFile.size - 1) {
            val lastModDate = Date(listFile.get(i).lastModified())
            val size = listFile.get(i).length()
            var name: String
            name = listFile.get(i).path.substring(
                listFile.get(i).path.lastIndexOf("/") + 1,
                listFile.get(i).path.length
            ).replace(".jpg", "")
            var media: Media = Photo("${lastModDate.time}", name, listFile.get(i).path, size, 0)
            imageList.add(media)
            updateHandler(REQUEST_UPDAT_PHOTO_LIST)
        }
    }


    fun renameImage(photo: Photo, newName: String) {
        val currentFile = File(photo.path)
        val newFile = File(ConstantUtils.BASE_FILE + "/image/$newName.jpg")
        var rs = currentFile.renameTo(newFile)
        mCallBack.renameSuccess()
    }

    fun deleteImage(photo: Photo) {
        val file = File(photo.path)
        if (file.exists() && file.isFile) {
            file.delete()
        }
        mCallBack.deleteSuccess()
    }

    override fun excuteHandler(msg: Message) {
        if (msg.what.equals(REQUEST_UPDAT_PHOTO_LIST)) {
            mCallBack.onResultPhoto(imageList)
        }
    }
}
