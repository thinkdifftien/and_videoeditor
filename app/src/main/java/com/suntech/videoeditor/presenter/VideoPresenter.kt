package com.suntech.recorder.presenter

import android.media.MediaMetadataRetriever
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Message
import android.provider.MediaStore
import android.util.Log

import com.suntech.recorder.event.OnActionVideo
import com.suntech.recorder.model.Media
import com.suntech.recorder.model.Video
import com.suntech.recorder.utils.ConstantUtils
import com.suntech.videoeditor.App
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class VideoPresenter(mCallBack: OnActionVideo) : BasePresenter<OnActionVideo>(mCallBack) {
    private var videoList: MutableList<Media> = ArrayList()
    private val REQUEST_UPDAT_VIDEO_LIST: Int = 1001

    fun getRecordVideoList() {
        Thread(Runnable {
            excuteLoadVideoList()
        }).start()
    }

    private fun excuteLoadVideoList() {
        videoList.clear()
        val file = File(ConstantUtils.BASE_FILE + "/video")
        Log.v("path",file.absolutePath.toString())
        if (!file.exists()) {
            Log.v("path!",file.absolutePath.toString())
            file.mkdir()
        }
        val listFile = file.listFiles()
        listFile ?: return
        listFile.reverse()
        for (i in 0..listFile.size - 1) {
            val thumbnail = ThumbnailUtils.createVideoThumbnail(
                listFile.get(i).path,
                MediaStore.Video.Thumbnails.MICRO_KIND
            )
            val lastModDate = Date(listFile.get(i).lastModified())
            val time = getTime(listFile.get(i).path)
            val size = listFile.get(i).length()
            if (time == 0L || size == 0L) {
                continue
            }
            var name: String
            name = listFile.get(i).path.substring(
                listFile.get(i).path.lastIndexOf("/") + 1,
                listFile.get(i).path.length
            ).replace(".mp4", "")
            val media: Media = Video(
                thumbnail,
                time,
                "${lastModDate.time}",
                name,
                listFile.get(i).path,
                size,
                lastModDate.time
            )
            videoList.add(media)
            updateHandler(REQUEST_UPDAT_VIDEO_LIST)
        }
    }


    private fun getTime(path: String): Long {
        var retriever = MediaMetadataRetriever();
        val timeInMillisec = try {
            retriever.setDataSource(App.instance, Uri.fromFile(File(path)));
            val time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
            retriever.release()
            time.toLong()
        } catch (e: Exception) {
            return 0
        }
        return timeInMillisec
    }



    fun deleteVideo(video: Video) {
        val file = File(video.path)
        if (file.exists() && file.isFile) {
            file.delete()
        }
        mCallBack.deleteSuccess()
    }

    fun sortByDate() {
        var video1: Video
        var video2: Video
        var temp: Video
        for (i in 0..videoList.size - 1) {
            for (j in 1..videoList.size - 1) {
                video1 = videoList.get(j - 1) as Video
                video2 = videoList.get(j) as Video
                if (video1.date > video2.date) { //swap elements
                    temp = video1
                    videoList.set(j - 1, video2)
                    videoList.set(j, temp)
                }
            }
        }
        mCallBack.onResultVideoList(videoList)
    }

    fun sortByCapacity() {
        var temp: Video
        var video1: Video
        var video2: Video
        for (i in 0..videoList.size - 1) {
            for (j in 1..videoList.size - 1) {
                video1 = videoList.get(j - 1) as Video
                video2 = videoList.get(j) as Video
                if (video1.size < video2.size) { //swap elements
                    temp = video1
                    videoList.set(j - 1, video2)
                    videoList.set(j, temp)
                }
            }
        }
        mCallBack.onResultVideoList(videoList)
    }


    override fun excuteHandler(msg: Message) {
        if (msg.what == REQUEST_UPDAT_VIDEO_LIST) {
            mCallBack.onResultVideoList(videoList)
        }
    }
}
