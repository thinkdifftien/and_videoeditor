package com.suntech.videoeditor

import android.app.Application
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import com.cloudinary.android.MediaManager
import com.suntech.recorder.StorageCommon
import com.suntech.recorder.utils.AudienceNetworkUtils


class App : Application() {
    companion object {
        lateinit var instance: App
        lateinit var audienceNetworkUtils: AudienceNetworkUtils
        lateinit var storageCommon: StorageCommon
        lateinit var admod: Admod

    }

    override fun onCreate() {
        super.onCreate()
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        instance = this
        audienceNetworkUtils = AudienceNetworkUtils()
        storageCommon = StorageCommon()
        admod = Admod()
        MediaManager.init(this)
    }
}