package com.suntech.videoeditor.interfaces;

import android.app.Activity;

import com.hw.photomovie.render.GLTextureView;
import com.suntech.videoeditor.widget.FilterItem;
import com.suntech.videoeditor.widget.TransferItem;


import java.util.List;

/**
 * Created by huangwei on 2018/9/9.
 */
public interface ISlideView {
    public GLTextureView getGLView();
    void setFilters(List<FilterItem> filters);
    Activity getActivity();

    void setTransfers(List<TransferItem> items);
}
