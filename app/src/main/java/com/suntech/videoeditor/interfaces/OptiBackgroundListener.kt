package com.suntech.videoeditor.interfaces

interface OptiBackgroundListener  {
    fun selectedBackground(path: String)
}