package com.suntech.recorder.interfaces

import android.view.View

interface AddFilterListener {
    fun onClick(v: View, position: Int)
}
