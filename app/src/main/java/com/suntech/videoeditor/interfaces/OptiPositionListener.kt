/*
 *
 *  Created by Optisol on Aug 2019.
 *  Copyright © 2019 Optisol Business Solutions pvt ltd. All rights reserved.
 *
 */

package com.suntech.recorder.interfaces

interface OptiPositionListener {
    fun selectedPosition(position: String)
}