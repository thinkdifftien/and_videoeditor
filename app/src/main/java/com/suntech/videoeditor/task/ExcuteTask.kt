package com.suntech.recorder.task

import android.os.AsyncTask

class ExcuteTask : AsyncTask<Any, Any, Any>() {
    private lateinit var mCallBack: OnAsyncCallBack

    fun setmCallBack(mCallBack: OnAsyncCallBack) {
        this.mCallBack = mCallBack
    }

    override fun onPreExecute() {
        mCallBack.onPreExcute()
    }

    override fun doInBackground(vararg data: Any?): Any {
        val key = data[0] as Int
        return mCallBack.excuteTask(this, key, data[1])!!
    }

    override fun onProgressUpdate(vararg values: Any) {
        val key = values[0] as Int
        mCallBack.updateUI(key, values[1])
    }

    override fun onPostExecute(value: Any?) {
        mCallBack.taskComplete(value)
    }

    fun updateTask(vararg value: Any?) {
        publishProgress(value[0], value[1])
    }

    interface OnAsyncCallBack {
        fun excuteTask(
            myTask: ExcuteTask?,
            key: Int,
            data: Any?
        ): Any?

        fun taskComplete(value: Any?) {}
        fun updateUI(key: Int, value: Any?) {}
        fun onPreExcute() {
        }
    }


}