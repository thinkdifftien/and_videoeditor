package com.suntech.recorder.task

import android.hardware.display.DisplayManager
import android.hardware.display.VirtualDisplay
import android.media.MediaRecorder
import android.media.projection.MediaProjection
import android.os.Build
import android.view.Surface
import com.suntech.recorder.model.RecorderConfig
import com.suntech.recorder.utils.ConstantUtils
import com.suntech.recorder.utils.SettingUtils
import com.suntech.videoeditor.App
import java.io.File
import java.io.IOException

class RecordManager(config: RecorderConfig?) {

    val BASE_FILE = ConstantUtils.BASE_FILE + "/video"
    val EXTENSION = ".mp3"
    val EXTENSION_VIDEO = ".mp4"
    private var mMediaRecorder: MediaRecorder? = null
    private var virtualDisplay: VirtualDisplay? = null
    private var config: RecorderConfig = config!!


    init {
        val file = File(BASE_FILE)
        if (!file.exists()) {
            file.mkdir()
        }
    }

    private fun intiAudioRecorder(fileName: String) {
        val outputFile = "$BASE_FILE/$fileName$EXTENSION"
        mMediaRecorder = MediaRecorder()
        mMediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mMediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mMediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mMediaRecorder!!.setOutputFile(outputFile)
        try {
            mMediaRecorder!!.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun startAudioRecord(fileName: String) {
        intiAudioRecorder(fileName)
        try {
            mMediaRecorder!!.start()
        } catch (ioe: Exception) {
        }
    }

    fun stopRecording() {
        try {
            mMediaRecorder!!.stop()
            mMediaRecorder!!.release()
            mMediaRecorder = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun startScreenRecord(
        mScreenDensity: Int,
        fileName: String
    ) {
        initScreenRecorder(fileName)
        virtualDisplay =
            createVirtualDisplay(App.storageCommon.mMediaProjectionRecorder, mScreenDensity)
        mMediaRecorder!!.start()
    }

    fun createVirtualDisplay(
        mMediaProjection: MediaProjection?,
        mScreenDensity: Int
    ): VirtualDisplay? {
        return mMediaProjection?.createVirtualDisplay(
            "MainActivity",
            config.height, config.width, mScreenDensity,
            DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
            mMediaRecorder!!.surface, null /*Callbacks*/, null /*Handler*/
        )
    }

    fun createVirtualDisplay(
        mMediaProjection: MediaProjection?,
        mScreenDensity: Int,
        surface: Surface
    ): VirtualDisplay? {
        return mMediaProjection?.createVirtualDisplay(
            "MainActivity",
            config.height, config.width, mScreenDensity,
            DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
            surface, null /*Callbacks*/, null /*Handler*/
        )
    }


    fun stopScreenRecord() {
        if (virtualDisplay == null) {
            return
        }
        virtualDisplay!!.release()
        stopRecording()
    }

    private fun initScreenRecorder(fileName: String) {
        val outputFile = "$BASE_FILE/$fileName$EXTENSION_VIDEO"
        var file = File(outputFile)
        if (!file.exists()) {
            file.createNewFile()
        }
        try {
            mMediaRecorder = MediaRecorder()

            mMediaRecorder!!.setVideoSource(MediaRecorder.VideoSource.SURFACE)
            if (SettingUtils.instance.isSound()) {
                mMediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
            }
            mMediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
            mMediaRecorder!!.setOutputFile(outputFile)
            mMediaRecorder!!.setVideoSize(config.height, config.width)
            mMediaRecorder!!.setVideoEncoder(MediaRecorder.VideoEncoder.H264)
            if (SettingUtils.instance.isSound()) {
                mMediaRecorder!!.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            }
            mMediaRecorder!!.setVideoEncodingBitRate(config.bitRate)
            mMediaRecorder!!.setVideoFrameRate(config.frameRate)
            mMediaRecorder!!.setOrientationHint(0)
            mMediaRecorder!!.prepare()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun pause() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mMediaRecorder?.pause()
        }
    }

    fun resume() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mMediaRecorder?.resume()
        }
    }

}