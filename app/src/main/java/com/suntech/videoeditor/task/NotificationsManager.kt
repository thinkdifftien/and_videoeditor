package com.suntech.recorder.task

import android.R
import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Build
import android.os.Build.VERSION_CODES
import android.os.SystemClock
import android.text.format.DateUtils

internal class NotificationsManager(context: Context?) :
    ContextWrapper(context) {
    private var mLastFiredTime: Long = 0
    private var mManager: NotificationManager? = null
    private var mStopAction: Notification.Action? = null
    private var mBuilder: Notification.Builder? = null

    fun recording(timeMs: Long) {
        if (SystemClock.elapsedRealtime() - mLastFiredTime < 1000) {
            return
        }
        val notification = builder?.setContentText(
            "length video " + " " + DateUtils.formatElapsedTime(
                timeMs / 1000
            )
        )?.build()
        notificationManager!!.notify(id, notification)
        mLastFiredTime = SystemClock.elapsedRealtime()
    }

    private val builder: Notification.Builder?
        private get() {
            if (mBuilder == null) {
                val builder: Notification.Builder = Notification.Builder(this)
                    .setContentTitle("gravando")
                    .setOngoing(true)
                    .setLocalOnly(true)
                    .setOnlyAlertOnce(true)
                    .addAction(stopAction())
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.ic_notification_overlay)
                if (Build.VERSION.SDK_INT >= VERSION_CODES.O) {
                    builder.setChannelId(CHANNEL_ID)
                        .setUsesChronometer(true)
                }
                mBuilder = builder
            }
            return mBuilder
        }

    @TargetApi(VERSION_CODES.O)
    private fun createNotificationChannel() {
        val channel =
            NotificationChannel(
                CHANNEL_ID,
                CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW
            )
        channel.setShowBadge(false)
        notificationManager!!.createNotificationChannel(channel)
    }

    private fun stopAction(): Notification.Action {
//        if (mStopAction == null) {
//            val intent: Intent = Intent(ACTION_STOP).setPackage(packageName)
//            val pendingIntent = PendingIntent.getBroadcast(
//                this, 1,
//                intent, PendingIntent.FLAG_ONE_SHOT
//            )
//            mStopAction = Notification.Action(
//                R.drawable.ic_media_pause,
//                "Stop",
//                pendingIntent
//            )
//        }
        return mStopAction!!
    }


    fun clear() {
        mLastFiredTime = 0
        mBuilder = null
        mStopAction = null
        notificationManager!!.cancelAll()
    }

    val notificationManager: NotificationManager?
        get() {
            if (mManager == null) {
                mManager =
                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            }
            return mManager
        }

    companion object {
        private const val id = 0x1fff
        private const val CHANNEL_ID = "Recording"
        private const val CHANNEL_NAME = "Screen Recorder NotificationsManager"
    }

    init {
        if (Build.VERSION.SDK_INT >= VERSION_CODES.O) {
            createNotificationChannel()
        }
    }
}
