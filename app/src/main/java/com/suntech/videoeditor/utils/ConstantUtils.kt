package com.suntech.recorder.utils

import com.suntech.videoeditor.App

class ConstantUtils {
    companion object {
        const val APP_NAME = "ScreenRecorder"
        const val FONT = ".Font"
        const val DEFAULT_FONT = "roboto_black.ttf"

        val HIDE_MEDIA: Int=106
        val UPDATE_WHEN_PAUSING: Int=105
        val UPDATE_TIME_RECORD: Int = 104
        val UPDATE_MENU_RECORDER: Int = 103
        val BASE_FILE = App.instance.getExternalFilesDir(null)?.path
        val KEY_LOAD_ANIM = "KEY_LOAD_ANIM"
        val COUNT_DOWN: Int = 102
        val RECORDING: Int = 101
        val UPDATE_MENU: Int = 100

        const val DATE_FORMAT = "yyyyMMdd_HHmmss"
        const val VIDEO_FORMAT = ".mp4"
        const val CLIP_ARTS = ".ClipArts"
        const val BOTTOM_LEFT = "BottomLeft"
        const val BOTTOM_RIGHT = "BottomRight"
        const val CENTRE = "Center"
        const val CENTRE_ALIGN = "CenterAlign"
        const val CENTRE_BOTTOM = "CenterBottom"
        const val TOP_LEFT = "TopLeft"
        const val TOP_RIGHT = "TopRight"


        const val VIDEO_FLIRT = 1
        const val VIDEO_TRIM = 2
        const val AUDIO_TRIM = 3
        const val VIDEO_AUDIO_MERGE = 4
        const val VIDEO_PLAYBACK_SPEED = 5
        const val VIDEO_TEXT_OVERLAY = 6
        const val VIDEO_CLIP_ART_OVERLAY = 7
        const val MERGE_VIDEO = 8
        const val VIDEO_TRANSITION = 9
        const val CONVERT_AVI_TO_MP4 = 10
        const val VIDEO_MERGE_1 = 104
        const val VIDEO_MERGE_2 = 105
        const val VIDEO_BACKGROUND_OVERLAY = 11

        var TYPE_GIF = "gif"
        var FOLDER_STICKER = "/unzipFolder/sticker/"
        const val TYPE_DOWLOAD = "TYPE_DOWNLOAD"
        const val TYPE_ASSETS = "TYPE_ASSETS"

    }
}