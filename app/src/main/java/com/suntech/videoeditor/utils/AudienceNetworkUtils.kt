package com.suntech.recorder.utils

import android.content.Context
import android.widget.FrameLayout
import com.facebook.ads.*
import com.facebook.ads.AdSize
import com.suntech.videoeditor.App


class   AudienceNetworkUtils {
    private val ID_INTERSTITIAL_AD = "524353388154469_524353624821112"
    private val ID_NATIVE_AD = "524353388154469_527398557849952"
    private val ID_BANNER_AD = "1324982201005084_1677291522440815"

    private lateinit var mInterstitialAd: InterstitialAd
    private val NUM_SHOW_ADS = 4
    private var numClicked = 4

    init {
        AudienceNetworkAds.initialize(App.instance)
        initInterstitial(App.instance)
    }

    fun loadBanner(context: Context, frame: FrameLayout) {
        var adView =
            AdView(context, ID_BANNER_AD, AdSize.BANNER_HEIGHT_50)
        adView.loadAd()
        frame.addView(adView)
    }

    private fun initInterstitial(mContext: Context) {
        mInterstitialAd = InterstitialAd(mContext, ID_INTERSTITIAL_AD)
        mInterstitialAd.setAdListener(object : InterstitialAdListener {
            override fun onInterstitialDisplayed(ad: Ad) { // Interstitial ad displayed callback
            }

            override fun onInterstitialDismissed(ad: Ad) { // Interstitial dismissed callback
                initInterstitial(mContext)
            }

            override fun onError(ad: Ad, adError: AdError) {

            }

            override fun onAdLoaded(ad: Ad) { // Show the ad
            }

            override fun onAdClicked(ad: Ad) { // Ad clicked callback
            }

            override fun onLoggingImpression(ad: Ad) { // Ad impression logged callback
            }
        })
        mInterstitialAd.loadAd()
    }

    fun loadInterialAds() {
        if (numClicked < NUM_SHOW_ADS) {
            numClicked++;
            return
        }
        if (mInterstitialAd.isAdLoaded()) {
            numClicked = 0;
            mInterstitialAd.show();
        }
    }
}