package com.suntech.recorder.utils


class SettingUtils {

    companion object {
        val instance = SettingUtils()
        val ORIENTATION_HORIRONTAL: String = "ORIENTATION_HORIRONTAL"
        val RESOLUTION_WIDTH = "RESOLUTION_WIDTH"
        val RESOLUTION_HEIGHT = "RESOLUTION_HEIGHT"
        val QUALITY = "QUALITY"
        val RECORD_SPEED = "RECORD_SPEED"
        val SOUNDS = "SOUNDS"
        val CAMERA = "CAMERA"
        val RECORD_TIME = "RECORD_TIME"
        val COUNT_DOWN_BEFORE_START = "COUNT_DOWN_BEFORE_START"
        val COUNT_DOWN_VALUE = "COUNT_DOWN_VALUE"
    }

    fun setResolution(resolution: IntArray) {
        var width = resolution[0]
        var height = resolution[1]
        CommonUtils.instance.savePref(RESOLUTION_WIDTH, "$width")
        CommonUtils.instance.savePref(RESOLUTION_HEIGHT, "$height")
    }

    fun getResolution(): IntArray {
        var width = CommonUtils.instance.getValuePref(RESOLUTION_WIDTH, "1280")!!.toInt()
        var height = CommonUtils.instance.getValuePref(RESOLUTION_HEIGHT, "720")!!.toInt()
        return intArrayOf(width, height)
    }

    fun setQuality(qua: Int) {
        CommonUtils.instance.savePref(QUALITY, "$qua")
    }

    fun getQuality(): Int {
        return CommonUtils.instance.getValuePref(QUALITY, "6000000")!!.toInt()
    }

    fun setRecordSpeed(speed: Int) {
        CommonUtils.instance.savePref(RECORD_SPEED, "$speed")
    }

    fun getRecordSpeed(): Int {
        return CommonUtils.instance.getValuePref(RECORD_SPEED, "30")!!.toInt()
    }

    fun setSounds(isSound: Boolean) {
        CommonUtils.instance.savePref(SOUNDS, "$isSound")
    }

    fun isSound(): Boolean {
        return CommonUtils.instance.getValuePref(SOUNDS, "true")!!.toBoolean()
    }

    fun setCamera(isCamera: Boolean) {
        CommonUtils.instance.savePref(CAMERA, "$isCamera")
    }

    fun isCamera(): Boolean {
        return CommonUtils.instance.getValuePref(CAMERA, "false")!!.toBoolean()
    }

    fun setRecordTime(isRecordTime: Boolean) {
        CommonUtils.instance.savePref(RECORD_TIME, "$isRecordTime")
    }

    fun isRecordTime(): Boolean {
        return CommonUtils.instance.getValuePref(RECORD_TIME, "true")!!.toBoolean()
    }

    fun setCountDownBeforeStart(isCountDown: Boolean) {
        CommonUtils.instance.savePref(COUNT_DOWN_BEFORE_START, "$isCountDown")
    }

    fun isCountDownBeforeStart(): Boolean {
        return CommonUtils.instance.getValuePref(COUNT_DOWN_BEFORE_START, "true")!!.toBoolean()
    }

    fun setCountDownValue(countDownValue: Int) {
        CommonUtils.instance.savePref(COUNT_DOWN_VALUE, "$countDownValue")
    }
    fun getCountDownValue(): Int {
        return CommonUtils.instance.getValuePref(COUNT_DOWN_VALUE, "3")!!.toInt()
    }

    fun setHorirontal(checked: Boolean) {
        CommonUtils.instance.savePref(ORIENTATION_HORIRONTAL, "$checked")
    }

    fun isHorirontal(): Boolean {
        return CommonUtils.instance.getValuePref(ORIENTATION_HORIRONTAL, "false")!!.toBoolean()
    }

}