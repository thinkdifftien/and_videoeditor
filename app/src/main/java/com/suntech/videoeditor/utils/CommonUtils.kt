package com.suntech.recorder.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Environment
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.suntech.recorder.view.event.OnActionCallBack
import com.suntech.recorder.view.event.OnCommonCallBack
import com.suntech.videoeditor.App
import com.suntech.videoeditor.R
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.InputStream
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class CommonUtils {

    private val FILE_SETTING: String = "setting.pref"
    private val PUBLISH_NAME: String = "Suntech Ltd."
    private val EMAIL: String = "suntectltd.software@gmail.com"
    private val SUBJECT: String = "FeedBack"
    private val TAG: String = CommonUtils::class.java.name

    companion object {
        val instance = CommonUtils()
    }

    fun loadAnim(view: View, anim_file: Int) {
        val anim: Animation = AnimationUtils.loadAnimation(App.instance, anim_file)
        view.startAnimation(anim)
    }

    fun loadAnim(view: View, anim_file: Int, callBack: OnActionCallBack) {
        val anim: Animation = AnimationUtils.loadAnimation(App.instance, anim_file)
        anim.setAnimationListener(object : OnCommonCallBack() {
            override fun onAnimationEnd(animation: Animation?) {
                callBack.callBack(ConstantUtils.KEY_LOAD_ANIM, null)
            }
        })
        view.startAnimation(anim)
    }

    fun toast(text: String) {
        Toast.makeText(App.instance, text, Toast.LENGTH_LONG).show()
    }

    fun log(text: String) {
        Log.d(TAG, text)
    }

    fun shareApp(context: Context) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        val shareBody =
            "https://play.google.com/store/apps/details?id=" + context.packageName.trim { it <= ' ' }
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, SUBJECT)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        context.startActivity(Intent.createChooser(sharingIntent, "Share to"))
    }

    fun support(context: Context) {
        val mailIntent = Intent(Intent.ACTION_VIEW)
        val data =
            Uri.parse("mailto:?SUBJECT=" + SUBJECT + "&body=" + "" + "&to=" + EMAIL)
        mailIntent.data = data
        context.startActivity(Intent.createChooser(mailIntent, "Send mail..."))
    }

    fun policy(context: Context,type : Int) {
        val dialog = Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        dialog.setContentView(R.layout.policy)
        val txt = dialog.findViewById<TextView>(R.id.content)
        val title = dialog.findViewById<TextView>(R.id.title)
        if (type==0){
            txt.setText(R.string.policy)
            title.setText("Policy")
        }
        else if(type==1){
            txt.setText(R.string.about)
            title.setText("About")
        }
        val close = dialog.findViewById<ImageView>(R.id.close).setOnClickListener {
            dialog.dismiss()
        }
        txt.setMovementMethod(ScrollingMovementMethod())
        dialog.show()
    }
    fun rateApp(context: Context) {
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + context.packageName)
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + context.packageName)
                )
            )
        }
    }

    fun moreApp(context: Context) {
        try {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://search?q=pub:" + PUBLISH_NAME)
                )
            )
        } catch (anfe: ActivityNotFoundException) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/developer?id=" + PUBLISH_NAME)
                )
            )
        }
    }

    fun readFileFromAssets(fileName: String): String {
        var data = ""
        try {
            val fin: InputStream = App.instance.getAssets().open(fileName)
            val buff = ByteArray(1024)
            var len: Int = fin.read(buff)
            while (len > 0) {
                data += String(buff, 0, len)
                len = fin.read(buff)
            }
            fin.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return data
    }

    fun getFileFromAssets(name: String): InputStream {
        return App.instance.getAssets().open(name)
    }

    /*
    * Save File
    * */

    fun saveFileToSystemStorage(file: File, nameFile: String) {
        val savePath =
            Environment.getDataDirectory().toString() + "/data/" + App.instance.getPackageName()
        val fin = FileInputStream(file) as InputStream
        saveFile(fin, savePath, nameFile)
    }


    fun saveFileToSystemStorage(fin: InputStream, nameFile: String) {
        val savePath =
            Environment.getDataDirectory()
                .toString() + "/data/" + App.instance.getPackageName() + "/$nameFile"
        saveFile(fin, savePath, nameFile)
    }


    private fun saveFile(fin: InputStream, savePath: String, nameFile: String) {
        val file = File(savePath)
        if (!file.exists()) {
            file.mkdirs()
        }
        val fout = FileOutputStream(File(savePath + "/$nameFile"))
        var lenght = 0
        val buff = ByteArray(1024)
        lenght = fin.read(buff)
        while (lenght > 0) {
            fout.write(buff, 0, lenght)
            lenght = fin.read(buff)
        }
        fin.close()
        fout.close()
    }

    fun saveFileToStorage(fin: InputStream, folder: String?, nameFile: String) {
        val savePath: String =
            ConstantUtils.BASE_FILE + "/$folder"
        saveFile(fin, savePath, nameFile)
    }

    fun saveFileToStorage(file: File, folder: String?, nameFile: String) {
        val savePath: String =
            ConstantUtils.BASE_FILE + "/$folder"
        val fin = FileInputStream(file) as InputStream
        saveFile(fin, savePath, nameFile)
    }

    fun convertFileToInputStream(file: File): InputStream {
        return FileInputStream(file) as InputStream
    }

    fun savePref(key: String, value: String) {
        var pref = App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE)
        pref.edit().putString(key, value).apply()
    }

    @SuppressLint("WrongConstant")
    fun savePref(key: String, value: String, isOverride: Boolean) {
        var pref: SharedPreferences
        if (!isOverride) {
            pref = App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE)
        } else {
            pref = App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_APPEND)
        }
        pref.edit().putString(key, value).apply()
    }

    fun getValuePref(key: String): String? {
        return App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE)
            .getString(key, "");
    }

    fun getValuePref(key: String, defaultValue: String): String? {
        return App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE)
            .getString(key, defaultValue);
    }

    fun clearPref() {
        var pref = App.instance.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE)
        pref.edit().clear().apply()
    }

    fun shareFile(context: Context, myFile: String) {
        val shareIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, Uri.fromFile(File(myFile)))
            type = "*/*"
        }
        context.startActivity(
            Intent.createChooser(
                shareIntent,
                App.instance.getText(R.string.txt_send_to)
            )
        )
    }

    fun formatTime(duration: Long): CharSequence? {
        val formatter: DateFormat = SimpleDateFormat("HH:mm:ss")
        val date = Date(duration)
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        return formatter.format(date)
    }

    fun formatTime(duration: Long, isHour: Boolean): CharSequence? {
        var formatter: DateFormat = SimpleDateFormat("HH:mm:ss")
        if (!isHour) {
            formatter = SimpleDateFormat("mm:ss")
        }
        val date = Date(duration)
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        return formatter.format(date)
    }

    private val K: Long = 1024
    private val M = K * K
    private val G = M * K
    private val T = G * K

    fun formatSize(value: Long): String? {
        val dividers =
            longArrayOf(T.toLong(), G, M.toLong(), K, 1)
        val units = arrayOf("TB", "GB", "MB", "KB", "B")
        require(value >= 1) { "Invalid file size: $value" }
        var result: String? = null
        for (i in dividers.indices) {
            val divider = dividers[i]
            if (value >= divider) {
                result = format(value, divider, units[i])
                break
            }
        }
        return result
    }

    private fun format(
        value: Long,
        divider: Long,
        unit: String
    ): String? {
        val result =
            if (divider > 1) value.toDouble() / divider.toDouble() else value.toDouble()
        return DecimalFormat("#,##0.#").format(result).toString() + " " + unit
    }

}