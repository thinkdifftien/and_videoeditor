package com.suntech.videoeditor.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Data {
    @SerializedName("sticker")
    @Expose
    private var sticker: Sticker? = null

    @SerializedName("background")
    @Expose
    private var backGround: BackGround? = null

    fun getSticker(): Sticker? {
        return sticker
    }

    fun setSticker(sticker: Sticker?) {
        this.sticker = sticker
    }

    fun getBackGround(): BackGround? {
        return backGround
    }

    fun setBackGround(backGround: BackGround?) {
        this.backGround = backGround
    }
}