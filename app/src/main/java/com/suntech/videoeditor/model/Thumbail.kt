package com.suntech.videoeditor.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName

class Thumbail {

    @SerializedName("id")
    @Expose
    private var id: String? = null
    @SerializedName("title")
    @Expose
    private var title: String? = null
    @SerializedName("username")
    @Expose
    private var username: String? = null
    @SerializedName("user_id")
    @Expose
    private var userId: String? = null
    @SerializedName("wallpaper")
    @Expose
    private var wallpaper: String? = null
    @SerializedName("thumbnail")
    @Expose
    private var thumbnail: String? = null
    @SerializedName("thumbnail_low")
    @Expose
    private var thumbnailLow: String? = null
    @SerializedName("download_count")
    @Expose
    private var downloadCount: String? = null
    @SerializedName("likes_count")
    @Expose
    private var likesCount: String? = null
    @SerializedName("original_src")
    @Expose
    private var originalSrc: String? = null
    @SerializedName("width")
    @Expose
    private var width: String? = null
    @SerializedName("height")
    @Expose
    private var height: String? = null
    @SerializedName("date_added")
    @Expose
    private var dateAdded: String? = null
    @SerializedName("tags")
    @Expose
    private var tags: String? = null
    @SerializedName("featured")
    @Expose
    private var featured: String? = null
    @SerializedName("active")
    @Expose
    private var active: String? = null

    fun getId(): String? {
        return id
    }

    fun setId(id: String?) {
        this.id = id
    }

    fun getTitle(): String? {
        return title
    }

    fun setTitle(title: String?) {
        this.title = title
    }

    fun getUsername(): String? {
        return username
    }

    fun setUsername(username: String?) {
        this.username = username
    }

    fun getUserId(): String? {
        return userId
    }

    fun setUserId(userId: String?) {
        this.userId = userId
    }

    fun getWallpaper(): String? {
        return wallpaper
    }

    fun setWallpaper(wallpaper: String?) {
        this.wallpaper = wallpaper
    }

    fun getThumbnail(): String? {
        return thumbnail
    }

    fun setThumbnail(thumbnail: String?) {
        this.thumbnail = thumbnail
    }

    fun getThumbnailLow(): String? {
        return thumbnailLow
    }

    fun setThumbnailLow(thumbnailLow: String?) {
        this.thumbnailLow = thumbnailLow
    }

    fun getDownloadCount(): String? {
        return downloadCount
    }

    fun setDownloadCount(downloadCount: String?) {
        this.downloadCount = downloadCount
    }

    fun getLikesCount(): String? {
        return likesCount
    }

    fun setLikesCount(likesCount: String?) {
        this.likesCount = likesCount
    }

    fun getOriginalSrc(): String? {
        return originalSrc
    }

    fun setOriginalSrc(originalSrc: String?) {
        this.originalSrc = originalSrc
    }

    fun getWidth(): String? {
        return width
    }

    fun setWidth(width: String?) {
        this.width = width
    }

    fun getHeight(): String? {
        return height
    }

    fun setHeight(height: String?) {
        this.height = height
    }

    fun getDateAdded(): String? {
        return dateAdded
    }

    fun setDateAdded(dateAdded: String?) {
        this.dateAdded = dateAdded
    }

    fun getTags(): String? {
        return tags
    }

    fun setTags(tags: String?) {
        this.tags = tags
    }

    fun getFeatured(): String? {
        return featured
    }

    fun setFeatured(featured: String?) {
        this.featured = featured
    }

    fun getActive(): String? {
        return active
    }

    fun setActive(active: String?) {
        this.active = active
    }
}