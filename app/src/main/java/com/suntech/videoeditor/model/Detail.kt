package com.suntech.videoeditor.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Detail {
    private var isdowload = false

    fun isIsdowload(): Boolean {
        return isdowload
    }

    fun setIsdowload(isdowload: Boolean) {
        this.isdowload = isdowload
    }

    @SerializedName("previewBackgroundColor")
    @Expose
    private var previewBackgroundColor: String? = null
    @SerializedName("IAPPriceFloat")
    @Expose
    private var iAPPriceFloat: Int? = null
    @SerializedName("bundleId")
    @Expose
    private var bundleId: String? = null
    @SerializedName("IAPItemId")
    @Expose
    private var iAPItemId: String? = null
    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("type")
    @Expose
    private var type: String? = null
    @SerializedName("payitems")
    @Expose
    private var payitems: String? = null
    @SerializedName("displayName")
    @Expose
    private var displayName: String? = null
    @SerializedName("icon")
    @Expose
    private var icon: String? = null
    @SerializedName("binaryData")
    @Expose
    private var binaryData: String? = null
    @SerializedName("mainCover")
    @Expose
    private var mainCover: MainCover? = null
    @SerializedName("squareMainCover")
    @Expose
    private var squareMainCover: SquareMainCover? = null
    @SerializedName("longMainCover")
    @Expose
    private var longMainCover: LongMainCover? = null
    @SerializedName("iconValue")
    @Expose
    private var iconValue: String? = null
    @SerializedName("detailmages")
    @Expose
    private var detailmages: MutableList<Detailmage> = ArrayList()
    @SerializedName("detailDescription")
    @Expose
    private var detailDescription: String? = null
    @SerializedName("isNew")
    @Expose
    private var isNew: Int? = null
    @SerializedName("isVip")
    @Expose
    private var isVip: Int? = null
    @SerializedName("colorful")
    @Expose
    private var colorful: Int? = null
    @SerializedName("unlockType")
    @Expose
    private var unlockType: Int? = null
    @SerializedName("videoUrl")
    @Expose
    private var videoUrl: String? = null
    @SerializedName("ver")
    @Expose
    private var ver: Int? = null
    @SerializedName("blendMode")
    @Expose
    private var blendMode: Int? = null
    @SerializedName("sloganBgColor")
    @Expose
    private var sloganBgColor: String? = null
    @SerializedName("marketType")
    @Expose
    private var marketType: Int? = null
    @SerializedName("materialType")
    @Expose
    private var materialType: Int? = null
    @SerializedName("marketUrl")
    @Expose
    private var marketUrl: String? = null
    @SerializedName("isLimitedFree")
    @Expose
    private var isLimitedFree: Int? = null
    @SerializedName("payInfo")
    @Expose
    private var payInfo: List<Any?>? = null

    fun getPreviewBackgroundColor(): String? {
        return previewBackgroundColor
    }

    fun setPreviewBackgroundColor(previewBackgroundColor: String?) {
        this.previewBackgroundColor = previewBackgroundColor
    }

    fun getIAPPriceFloat(): Int? {
        return iAPPriceFloat
    }

    fun setIAPPriceFloat(iAPPriceFloat: Int?) {
        this.iAPPriceFloat = iAPPriceFloat
    }

    fun getBundleId(): String? {
        return bundleId
    }

    fun setBundleId(bundleId: String?) {
        this.bundleId = bundleId
    }

    fun getIAPItemId(): String? {
        return iAPItemId
    }

    fun setIAPItemId(iAPItemId: String?) {
        this.iAPItemId = iAPItemId
    }

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getType(): String? {
        return type
    }

    fun setType(type: String?) {
        this.type = type
    }

    fun getPayitems(): String? {
        return payitems
    }

    fun setPayitems(payitems: String?) {
        this.payitems = payitems
    }

    fun getDisplayName(): String? {
        return displayName
    }

    fun setDisplayName(displayName: String?) {
        this.displayName = displayName
    }

    fun getIcon(): String? {
        return icon
    }

    fun setIcon(icon: String?) {
        this.icon = icon
    }

    fun getBinaryData(): String? {
        return binaryData
    }

    fun setBinaryData(binaryData: String?) {
        this.binaryData = binaryData
    }

    fun getMainCover(): MainCover? {
        return mainCover
    }

    fun setMainCover(mainCover: MainCover?) {
        this.mainCover = mainCover
    }

    fun getSquareMainCover(): SquareMainCover? {
        return squareMainCover
    }

    fun setSquareMainCover(squareMainCover: SquareMainCover?) {
        this.squareMainCover = squareMainCover
    }

    fun getLongMainCover(): LongMainCover? {
        return longMainCover
    }

    fun setLongMainCover(longMainCover: LongMainCover?) {
        this.longMainCover = longMainCover
    }

    fun getIconValue(): String? {
        return iconValue
    }

    fun setIconValue(iconValue: String?) {
        this.iconValue = iconValue
    }

    fun getDetailmages(): MutableList<Detailmage> {
        return detailmages
    }

    fun setDetailmages(detailmages: ArrayList<Detailmage>) {
        this.detailmages = detailmages
    }

    fun getDetailDescription(): String? {
        return detailDescription
    }

    fun setDetailDescription(detailDescription: String?) {
        this.detailDescription = detailDescription
    }

    fun getIsNew(): Int? {
        return isNew
    }

    fun setIsNew(isNew: Int?) {
        this.isNew = isNew
    }

    fun getIsVip(): Int? {
        return isVip
    }

    fun setIsVip(isVip: Int?) {
        this.isVip = isVip
    }

    fun getColorful(): Int? {
        return colorful
    }

    fun setColorful(colorful: Int?) {
        this.colorful = colorful
    }

    fun getUnlockType(): Int? {
        return unlockType
    }

    fun setUnlockType(unlockType: Int?) {
        this.unlockType = unlockType
    }

    fun getVideoUrl(): String? {
        return videoUrl
    }

    fun setVideoUrl(videoUrl: String?) {
        this.videoUrl = videoUrl
    }

    fun getVer(): Int? {
        return ver
    }

    fun setVer(ver: Int?) {
        this.ver = ver
    }

    fun getBlendMode(): Int? {
        return blendMode
    }

    fun setBlendMode(blendMode: Int?) {
        this.blendMode = blendMode
    }

    fun getSloganBgColor(): String? {
        return sloganBgColor
    }

    fun setSloganBgColor(sloganBgColor: String?) {
        this.sloganBgColor = sloganBgColor
    }

    fun getMarketType(): Int? {
        return marketType
    }

    fun setMarketType(marketType: Int?) {
        this.marketType = marketType
    }

    fun getMaterialType(): Int? {
        return materialType
    }

    fun setMaterialType(materialType: Int?) {
        this.materialType = materialType
    }

    fun getMarketUrl(): String? {
        return marketUrl
    }

    fun setMarketUrl(marketUrl: String?) {
        this.marketUrl = marketUrl
    }

    fun getIsLimitedFree(): Int? {
        return isLimitedFree
    }

    fun setIsLimitedFree(isLimitedFree: Int?) {
        this.isLimitedFree = isLimitedFree
    }

    fun getPayInfo(): List<Any?>? {
        return payInfo
    }

    fun setPayInfo(payInfo: List<Any?>?) {
        this.payInfo = payInfo
    }
}