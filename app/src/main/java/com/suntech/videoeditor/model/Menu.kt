package com.suntech.recorder.model

import android.graphics.Bitmap

open class Menu(thumbnail: Bitmap, name: String) {
    var thumbnail = thumbnail
    var name = name
}