package com.suntech.videoeditor.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BackGround {
    @SerializedName("list")
    @Expose
    private var list: List<Detail?>? = null
    @SerializedName("lastPage")
    @Expose
    private var lastPage: Int? = null

    fun getList(): List<Detail?>? {
        return list
    }

    fun setList(list: List<Detail?>?) {
        this.list = list
    }

    fun getLastPage(): Int? {
        return lastPage
    }

    fun setLastPage(lastPage: Int?) {
        this.lastPage = lastPage
    }
}