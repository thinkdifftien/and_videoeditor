package com.suntech.videoeditor.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Detailmage {

    @SerializedName("url")
    @Expose
    private var url: String? = null
    @SerializedName("height")
    @Expose
    private var height: Int? = null
    @SerializedName("width")
    @Expose
    private var width: Int? = null




    fun getUrl(): String? {
        return url
    }

    fun setUrl(url: String?) {
        this.url = url
    }

    fun getHeight(): Int? {
        return height
    }

    fun setHeight(height: Int?) {
        this.height = height
    }

    fun getWidth(): Int? {
        return width
    }

    fun setWidth(width: Int?) {
        this.width = width
    }
}