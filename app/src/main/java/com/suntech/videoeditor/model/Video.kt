package com.suntech.recorder.model

import android.graphics.Bitmap
import android.media.ThumbnailUtils
import java.time.Duration

class Video(
    thumbnail: Bitmap?,
    duration: Long,
    id: String,
    name: String,
    path: String,
    size: Long,
    date: Long
) :
    Media(id, name, path, size, date) {
    var thumbnail = thumbnail
    var duration = duration

}