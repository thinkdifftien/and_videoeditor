package com.suntech.recorder.model

class RecorderConfig(width: Int, height: Int, bitRate: Int, frameRate: Int, isSounds: Boolean) {
    var width = width
    var height = height
    var bitRate = bitRate
    var frameRate = frameRate
    var isSounds = isSounds
}