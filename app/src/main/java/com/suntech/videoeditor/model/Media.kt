package com.suntech.recorder.model

open class Media(
    var id: String,
    var name: String,
    var path: String,
    var size: Long,
    var date: Long
) {

}